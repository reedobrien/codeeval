package main

import "testing"

func TestCountArrows(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"0 0 1 5", "NE"},
		{"12 13 12 13", "here"},
		{"0 1 0 5", "N"},
	}
	for _, test := range table {
		got := countArrows(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
