// SUM OF PRIMES
// CHALLENGE DESCRIPTION:

// Write a program which determines the sum of the first 1000 prime numbers.

// INPUT SAMPLE:
// There is no input for this program.

// OUTPUT SAMPLE:
// Print to stdout the sum of the first 1000 prime numbers.

// 3682913
package main

import (
	"fmt"
	"math/big"
	"os"
)

var (
	nth, Σ int
)

func main() {
	nth = 3
	Σ = 2 + 3 + 5
	for i := 7; ; i += 2 {
		if big.NewInt(int64(i)).ProbablyPrime(20) {
			Σ += i
			nth++
			if nth == 1000 {
				fmt.Println(Σ)
				os.Exit(0)
			}
		}
	}
}
