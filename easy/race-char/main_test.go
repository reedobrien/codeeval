package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestRace(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"#########_##", "#########|##"},
		{"########C_##", "########/_##"},
		{"#######_####", "#######/####"},
		{"######_#C###", "######_#\\###"},
		{"#######_C###", "#######_|###"},
		{"#######_####", "#######/####"},
		{"######C#_###", "######/#_###"},
		{"######C_####", "######|_####"},
		{"#######_####", "#######\\####"},
		{"#######_####", "#######|####"},
	}
	for _, tv := range values {
		equals(t, race(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
