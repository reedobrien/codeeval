package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestDelta(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"Lorem ipsum dolor sit amet. Mea et habeo doming praesent. Te inani utroque recteque has, sea ne fugit verterem!", "Lorem ipsum dolor sit amet. Mea et habeo doming praesent, yeah! Te inani utroque recteque has, sea ne fugit verterem!"},
		{"Usu ei scripta phaedrum, an sed salutatus definiebas? Qui ut recteque gloriatur reformidans. Qui solum aeque sapientem cu.", "Usu ei scripta phaedrum, an sed salutatus definiebas, this is crazy, I tell ya. Qui ut recteque gloriatur reformidans. Qui solum aeque sapientem cu, can U believe this?"},
		{"Eu nam nusquam quaestio principes.", "Eu nam nusquam quaestio principes."},
	}
	for _, tv := range values {
		equals(t, spice(tv.in), tv.want)
	}
}

func TestP(t *testing.T) {
	values := []struct {
		times, want int
	}{
		{1, 1},
		{2, 2},
		{3, 3},
		{11, 3},
		{12, 4},
		{13, 5},
	}
	for _, tv := range values {
		var got pos
		for i := 0; i < tv.times; i++ {
			got.next()
		}
		equals(t, int(got), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
