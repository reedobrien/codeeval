package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"John Sara Tom Susan | 3", "Sara"},
		{"John Tom Mary | 5", "Mary"},
	}
	for i, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Errorf("test %d for %s got: %s wanted: %s", i, test.in, got, test.want)
		}
	}
}
