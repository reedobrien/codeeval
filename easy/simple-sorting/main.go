// Write a program which sorts numbers.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input
// example is the following
//
// 70.920 -38.797 14.354 99.323 90.374 7.581
// -37.507 -3.263 40.079 27.999 65.213 -55.552
//
// OUTPUT SAMPLE:
//
// Print sorted numbers in the following way. Please note, that you need to
// print the numbers till the 3rd digit after the dot including trailing zeros.
//
// -38.797 7.581 14.354 70.920 90.374 99.323
// -55.552 -37.507 -3.263 27.999 40.079 65.213
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

func SortEm(s string) string {
	var (
		result []string
		nums   []float64
	)
	digits := strings.Split(s, " ")
	for i := range digits {
		nums = append(nums, atof(digits[i]))
	}
	if !sort.Float64sAreSorted(nums) {
		sort.Float64s(nums)
	}
	for _, n := range nums {
		result = append(result, fmt.Sprintf("%.3f", n))
	}

	return strings.Join(result, " ")
}

func atof(s string) float64 {
	i, err := strconv.ParseFloat(s, 64)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting input to str:", err)
		os.Exit(1)
	}
	return i
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(SortEm(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
