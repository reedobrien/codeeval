package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestRoman(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"159", "CLIX"},
		{"296", "CCXCVI"},
		{"3992", "MMMCMXCII"},
		{"183", "CLXXXIII"},
		{"1349", "MCCCXLIX"},
	}
	for _, tv := range values {
		equals(t, convert(tv.in), tv.want)
	}
}

// equals fails the test if got is not equal to want.
func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot:  %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
