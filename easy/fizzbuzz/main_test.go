package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"testing"
)

func TestGame(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"3 5 10", "1 2 F 4 B F 7 8 F B"},
		{"2 7 15", "1 F 3 F 5 F B F 9 F 11 F 13 FB 15"},
		{"5 7 50", "1 2 3 4 F 6 B 8 9 F 11 12 13 B F 16 17 18 19 F B 22 23 24 F 26 27 B 29 F 31 32 33 34 FB 36 37 38 39 F 41 B 43 44 F 46 47 48 B F"},
		{"4 6 20", "1 2 3 F 5 B 7 F 9 10 11 FB 13 14 15 F 17 B 19 F"},
		{"1 2 10", "F FB F FB F FB F FB F FB"},
		{"7 3 21", "1 2 B 4 5 B F 8 B 10 11 B 13 F B 16 17 B 19 20 FB"},
	}
	for _, tv := range values {
		g := newGame(tv.in)
		equals(t, g.Play(), tv.want)
	}
}

func TestPlaygames(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"3 5 10\n2 7 15\n5 7 50", "1 2 F 4 B F 7 8 F B\n1 F 3 F 5 F B F 9 F 11 F 13 FB 15\n1 2 3 4 F 6 B 8 9 F 11 12 13 B F 16 17 18 19 F B 22 23 24 F 26 27 B 29 F 31 32 33 34 FB 36 37 38 39 F 41 B 43 44 F 46 47 48 B F\n"},
		{"4 6 20\n1 2 10\n7 3 21", "1 2 3 F 5 B 7 F 9 10 11 FB 13 14 15 F 17 B 19 F\nF FB F FB F FB F FB F FB\n1 2 B 4 5 B F 8 B 10 11 B 13 F B 16 17 B 19 20 FB\n"},
	}
	for _, tv := range values {
		f := strings.NewReader(tv.in)
		// Stdout capture stolen from here:
		// http://stackoverflow.com/questions/10473800/in-go-how-do-i-capture-stdout-of-a-function-into-a-string
		old := os.Stdout // keep backup of the real stdout
		r, w, _ := os.Pipe()
		os.Stdout = w
		playgames(f)
		outC := make(chan string)
		// copy the output in a separate goroutine so printing can't block indefinitely
		go func() {
			var buf bytes.Buffer
			io.Copy(&buf, r)
			outC <- buf.String()
		}()

		// back to normal state
		w.Close()
		os.Stdout = old // restoring the real stdout
		got := <-outC
		equals(t, got, tv.want)
	}
}

// equals fails the test if got is not equal to want.
func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot:  %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
