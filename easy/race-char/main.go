// Racing Chars

// Challenge Description:

// You are given a file where each line is a section of a race track with obstructions, gates, and checkpoints. Your task is to find a way to pass this track using the following information:

// 1. Each section contains either one single gate or one gate with a checkpoint.
// 2. You should drive only through gates or checkpoints.
// 3. You should drive through a checkpoint rather than a gate.
// 4. An obstruction is represented by a number sign "#".
// 5. A gate is represented by an underscore "_".
// 6. A checkpoint is represented by a letter C.

// Input sample:

// #########_##
// ########C_##
// #######_####
// ######_#C###
// #######_C###
// #######_####
// ######C#_###
// ######C_####
// #######_####
// #######_####

// Your program should accept a path to a filename as its first argument. Each line of the file is a new section of a race track.

// Output sample:

// #########|##
// ########/_##
// #######/####
// ######_#\###
// #######_|###
// #######/####
// ######/#_###
// ######|_####
// #######\####
// #######|####

// Print out the way of passing this race track starting from the first line in the file. Use a pipe "|" for the straight, use a slash "/" for the left turn, and use a backslash "\" for the right turn.
// Constraints:

//     The number of lines in a file is 50.
//     The width of a section is 12 characters.

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

var (
	d, m      string
	cur, last int
	first     bool = true
)

func race(l string) string {
	if strings.Contains(l, "C") {
		cur = strings.Index(l, "C")
		m = "C"
	} else {
		cur = strings.Index(l, "_")
		m = "_"
	}
	if !first {
		switch {
		case cur < last:
			d = "/"
		case cur > last:
			d = "\\"
		default:
			d = "|"
		}
	}
	if first {
		d = "|"
		first = false
	}
	l = strings.Replace(l, m, d, 1)
	last = cur
	return l
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(race(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
