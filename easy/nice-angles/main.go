// ICE ANGLES
// CHALLENGE DESCRIPTION:

// Write a program that outputs the value of angle, reducing its fractional
// part to minutes and seconds.

// INPUT SAMPLE:

// The first argument is a path to a file that contains the values of angles
// with their decimal fractions:

// 330.39991833
// 0.001
// 14.64530319
// 0.25
// 254.16991217

// OUTPUT SAMPLE:

// Print to stdout values of angles with their fractional parts reduced to
// minutes and seconds.

// The whole and fractional parts are separated by period, minutes are
// separated by apostrophe, seconds by double quotes. The values of minutes and
// seconds are shown as two numbers (with leading zeros if needed).

// 330.23'59"
// 0.00'03"
// 14.38'43"
// 0.15'00"
// 254.10'11"

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
)

func dangle(s string) string {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parseinf float from string:", err.Error())
	}
	return dToDSM(f)
}

func dToDSM(f float64) string {
	// d = integer(30.263888889º) = 30º
	// m = integer((dd - d) × 60) = 15'
	// s = (dd - d - m/60) × 3600 = 50"
	d := int(f)
	m := int((f - float64(d)) * 60)
	s := int((f - float64(d) - float64(m)/60) * 3600)
	return fmt.Sprintf("%0d.%02d'%02d\"", d, m, s)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(dangle(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
