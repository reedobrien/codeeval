// Max Range Sum

// Challenge Description:

// Bob is developing a new strategy to get rich in the stock market. He wishes to invest his portfolio for 1 or more days, then sell it at the right time to maximize his earnings. Bob has painstakingly tracked how much his portfolio would have gained or lost for each of the last N days. Now he has hired you to figure out what would have been the largest total gain his portfolio could have achieved.

// For example:

// Bob kept track of the last 10 days in the stock market. On each day, the gains/losses are as follows:

// 7 -3 -10 4 2 8 -2 4 -5 -2

// If Bob entered the stock market on day 4 and exited on day 8 (5 days in total), his gains would have been

// 16 (4 + 2 + 8 + -2 + 4)

// Input sample:

// The input contains N, the number of days (0 < N < 10000), followed by N (separated by symbol ";") integers D (-10000 < D < 10000) indicating the gain or loss on that day.

// For example:

// 5;7 -3 -10 4 2 8 -2 4 -5 -2
// 6;-4 3 -10 5 3 -7 -3 7 -6 3
// 3;-7 0 -45 34 -24 7

// Output sample:

// Print out the maximum possible gain over the period. If no gain is possible, print 0.

// For example:

// 16
// 0
// 17

// Constraints:

//     Gain or loss on that day is (-100 < D < 100).
//     Number of days (0 < N < 100).
//     Number of test cases is 20.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

var max, start, sum int

func msr(s string) string {
	days, values := parseVals(s)
	// reset to 0
	start = 0
	sum = 0
	max = 0
	sel := values[start : start+days]
	for _, i := range sel {
		sum += i
	}
	max = sum
	for i := start + days; i < len(values); i++ {
		sum = sum + values[i]
		sum = sum - values[i-days]
		if sum > max {
			max = sum
		}
	}
	if max < 0 {
		max = 0
	}
	return strconv.Itoa(max)
}

func parseVals(s string) (int, []int) {
	x := strings.Split(s, ";")
	days, err := strconv.Atoi(x[0])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing int from string:", err.Error())
		os.Exit(1)
	}
	vals := []int{}
	for _, v := range strings.Split(x[1], " ") {
		i, err := strconv.Atoi(v)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error parsing int from string:", err.Error())
			os.Exit(1)
		}
		vals = append(vals, i)
	}
	return days, vals
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(msr(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input from file:", err.Error())
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input file:", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
