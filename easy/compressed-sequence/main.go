// COMPRESSED SEQUENCE
// CHALLENGE DESCRIPTION:

// Assume that someone dictates you a sequence of numbers, and you need to
// write it down. For brevity, he dictates it as follows: first he dictates the
// number of consecutive identical numbers, and then - the number itself.

// For example:
// The sequence below

// 1 1 3 3 3 2 2 2 2 14 14 14 11 11 11 2

// is dictated as follows:

// two times one, three times three, four times two, three times fourteen,
// three times eleven, one time two and you have to write down the sequence

// 2 1 3 3 4 2 3 14 3 11 1 2

// Your task is to write a program that compresses a given sequence using this
// approach.

// INPUT SAMPLE:

// Your program should accept a path to a file as its first argument that
// contains T number of lines. Each line is a test case represented by a
// sequence of integers with the length L, where each integer is N separated by
// a space.

// 40 40 40 40 29 29 29 29 29 29 29 29 57 57 92 92 92 92 92 86 86 86 86 86 86 86 86 86 86
// 73 73 73 73 41 41 41 41 41 41 41 41 41 41
// 1 1 3 3 3 2 2 2 2 14 14 14 11 11 11 2
// 7

// OUTPUT SAMPLE:

// For each test case, print out a compressed sequence of numbers separated by
// a single space, one per line.

// For example:

// 4 40 8 29 2 57 5 92 10 86
// 4 73 10 41
// 2 1 3 3 4 2 3 14 3 11 1 2
// 1 7

// CONSTRAINTS:

// T is in a range from 20 to 50.
// N is in a range from 0 to 99.
// L is in a range from 1 to 400.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

func del(i int, a []string) []string {
	a = append(a[:i], a[i+1:]...)
	return a
}

func compress(l string) string {
	var (
		c, p string
		n    int
	)
	a := strings.Split(l, " ")
	out := []string{}
	for i := 0; ; {
		// start a 0 and get element
		c = a[i]
		if c == p {
			// if c == p this is a repeat so remove the index
			a = del(i, a)
		} else {
			if p != "" {
				// if c != p then this is a new item (if p is set)
				// append it and the count
				out = append(out, fmt.Sprintf("%d %s", n, p))
				// move to next element
				i++
				// reset the counter
				n = 0
			} else {
				i++
			}
		}
		// increment counter
		n++
		// set previous to current for next loop
		p = c
		if i >= len(a) {
			// if we are at the end of the slice, append this value and break
			out = append(out, fmt.Sprintf("%d %s", n, p))
			break
		}
	}
	// Return the output
	return strings.Join(out, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(compress(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
