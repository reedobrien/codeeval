package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"1 2 3 4 | 3 1 | 4 1", "1:1,2,3; 2:1; 3:1,2; 4:1,3;"},
		{"19 11 | 19 21 23 | 31 39 29", "11:1; 19:1,2; 21:2; 23:2; 29:3; 31:3; 39:3;"},
	}
	for _, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Fatalf("testing %q got %q, wanted %q", test.in, got, test.want)
		}
	}
}
