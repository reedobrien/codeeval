// Given two integers N and M, calculate N Mod M (without using any inbuilt modulus operator).
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Each line in this file contains two comma separated positive integers. E.g.
//
// 20,6
// 2,3
//
// You may assume M will never be zero.
//
// OUTPUT SAMPLE:
//
// Print out the value of N Mod M
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func modulo(n, m int) int {
	return n - m*(n/m)
}

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting numeral char to int:", err)
		os.Exit(1)
	}
	return i
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		elems := strings.Split(line, ",")
		fmt.Printf("%d\n", modulo(atoi(elems[0]), atoi(elems[1])))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
