// ROAD TRIP
// CHALLENGE DESCRIPTION:

// You've decided to make a road trip across the country in a straight line.
// You have chosen the direction you'd like to travel and made a list of cities
// in that direction that have gas stations to stop at and fill up your tank.
// To make sure that this route is viable, you need to know the distances
// between the adjacent cities in order to be able to travel this distance on a
// single tank of gasoline, (No one likes running out of gas.) but you only
// know distances to each city from your starting point.

// INPUT SAMPLE:

// The first argument is a path to a filename. Each line in the file contains
// the list of cities and distances to them, comma delimited, from the starting
// point of your trip. You start your trip at point 0. The cities with their
// distances are separated by semicolon. E.g.

// Rkbs,5453; Wdqiz,1245; Rwds,3890; Ujma,5589; Tbzmo,1303;
// Vgdfz,70; Mgknxpi,3958; Nsptghk,2626; Wuzp,2559; Jcdwi,3761;
// Yvnzjwk,5363; Pkabj,5999; Xznvb,3584; Jfksvx,1240; Inwm,5720;
// Ramytdb,2683; Voclqmb,5236;

// OUTPUT SAMPLE:

// Print out the distance from the starting point to the nearest city, and the
// distances between two nearest cities separated by comma, in order they
// appear on the route. E.g.

// 1245,58,2587,1563,136
// 70,2489,67,1135,197
// 1240,2344,1779,357,279
// 2683,2553

// Constrains:
// Cities are unique, and represented by randomly generated string containing
// latin characters [A-Z][a-z].  The route length is an integer in range
// [10000, 30000] The number of cities is in range [500, 600]
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

type city struct {
	Name     string
	Distance int
}

func newCity(s string) city {
	c := strings.Split(s, ",")
	d, err := strconv.Atoi(c[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting string to int:", err)
	}
	return city{c[0], d}
}

type itinerary []city

func newItinerary(s string) itinerary {
	s = strings.Trim(s, ";")
	relays := strings.Split(s, ";")
	i := itinerary{}
	for _, c := range relays {
		i = append(i, newCity(strings.TrimSpace(c)))
	}
	sort.Sort(i)
	return i
}

func (it itinerary) Len() int           { return len(it) }
func (it itinerary) Less(i, j int) bool { return it[i].Distance < it[j].Distance }
func (it itinerary) Swap(i, j int)      { it[i], it[j] = it[j], it[i] }

func distance(s string) string {
	i := newItinerary(s)
	var cur, next int
	out := []string{}
	for _, c := range i {
		cur = next
		next = c.Distance
		out = append(out, strconv.Itoa(next-cur))
	}
	return strings.Join(out, ",")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(distance(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
