// TRICK OR TREAT
// CHALLENGE DESCRIPTION:
//
// Everyone knows what Halloween is and how children love it. Children in
// costumes travel from house to house asking for treats with a phrase "Trick
// or treat". After that, they divide the treats equally among all. This year,
// they collected tons of candies, and need your help to share everything
// equally.
// You know that children receive different number of candies depending on
// their costume: vampire gets 3 candies from one house, zombie – 4 candies,
// and witch – 5 candies.
// That is, three children in three different costumes get 3+4+5=12 candies
// from one house.
//
// INPUT SAMPLE:
// The first argument is a path to a file. Each line includes a test case with
// number of vampires, zombies, witches, and houses that they visited.
// For example:
//
// Vampires: 1, Zombies: 1, Witches: 1, Houses: 1
// 			 3           4           5   = 12 *1 /3 = 4
// Vampires: 3, Zombies: 2, Witches: 1, Houses: 10
// 			 9			 8           5   = 22 * 10 / 6
// OUTPUT SAMPLE:
//
// You need to print number of candies that each child will get. If the number
// is not integer, round it to the lower: for example, if the resulting number
// is 13.666, round it to 13.
// For example:
//
// 4
// 36
// CONSTRAINTS:
// Number of vampires, zombies, witches, and houses can be from 0 to 100.
// If the final number of candies is not integer, round it to the lower.
// The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

var amts = map[string]int{"Vampires": 3, "Zombies": 4, "Witches": 5}

type data struct {
	m map[string]int
}

func (d *data) candiesPerChild() int {
	mult := d.m["Houses"]
	delete(d.m, "Houses")
	var candies, children int
	for k, v := range d.m {
		children += v
		candies += v * amts[k]
	}
	return candies * mult / children
}

func newVals(s string) data {
	inputs := strings.Split(s, ", ")
	if len(inputs) != 4 {
		log.Fatalf("invalid input: %s", s)
	}
	d := data{m: map[string]int{}}
	for _, val := range inputs {
		item := strings.Split(val, ": ")
		i, err := strconv.Atoi(item[1])
		if err != nil {
			log.Fatalf("failed to convert %s to int", item[1])
		}
		d.m[item[0]] = i
	}
	return d
}

func process(s string) string {
	d := newVals(s)
	return strconv.Itoa(d.candiesPerChild())
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		log.Fatalln("error opening file:", err)
	}
	procLines(f)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(process(line))
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln("error opening file:", err)
	}
}
