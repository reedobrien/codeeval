// String mask

// Challenge Description:

// You’ve got a binary code which has to be buried among words in order to
// unconsciously pass the cipher.
// Create a program that would cover the word with a binary mask. If, while
// covering, a letter finds itself as 1, you have to change its register to the
// upper one, if it’s 0, leave it as it is. Words are always in lower case and
// in the same row with the binary mask.
// Input sample:

// hello 11001
// world 10000
// cba 111

// The first argument is a path to a file. Each row contains a test case with a
// word and a binary code separated with space, inside of it. The length of
// each word is equal to the length of the binary code.

// For example:
// Output sample:

// HEllO
// World
// CBA

// Print the encrypted words without binary code.

// For example:
// Constraints:

//     Words are from 1 to 20 letters long.
//     The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func unmask(s string) string {
	parts := strings.Split(s, " ")
	ct, mask := strings.Split(parts[0], ""), strings.Split(parts[1], "")
	for i, c := range ct {
		if mask[i] == "1" {
			ct[i] = strings.ToUpper(c)
		}
	}
	return strings.Join(ct, "")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(unmask(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
