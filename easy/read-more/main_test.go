package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestDelta(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"Tom exhibited.", "Tom exhibited."},
		{"Amy Lawrence was proud and glad, and she tried to make Tom see it in her face - but he wouldn't look.", "Amy Lawrence was proud and glad, and... <Read More>"},
		{"Tom was tugging at a button-hole and looking sheepish.", "Tom was tugging at a button-hole and looking sheepish."},
		{"Two thousand verses is a great many - very, very great many.", "Two thousand verses is a great many -... <Read More>"},
		{"Tom's mouth watered for the apple, but he stuck to his work.", "Tom's mouth watered for the apple, but... <Read More>"},
	}
	for _, tv := range values {
		equals(t, trunc(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
