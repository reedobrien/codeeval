package main

import "testing"

func TestCountArrows(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"cat dog hello", "h *e **l ***l ****o"},
		{"stop football play", "f *o **o ***t ****b *****a ******l *******l"},
		{"music is my life", "m *u **s ***i ****c"},
	}
	for _, test := range table {
		got := stepwiseWord(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
