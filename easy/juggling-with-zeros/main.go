// JUGGLING WITH ZEROS CHALLENGE DESCRIPTION:

// In this challenge, you will deal with zero-based numbers. A zero-based
// number has the following form: "flag" "sequence of zeroes" "flag" "sequence
// of zeroes", and so on. The numbers are separated by a single space.

// 00 0 0 00 00 0 You have to convert zero-based numbers into integers. To do
// this, you need to perform the following steps:

// Convert a zero-based number into a binary form using the following rules: a)
// flag "0" means that the following sequence of zeroes should be appended to a
// binary string.

// b) flag "00" means that the following sequence of zeroes should be
// transformed into a sequence of ones and be appended to a binary string.

// 00 0 0 00 00 0 --> 1001
// Convert the obtained binary string into an integer.
// 1001 --> 9
// INPUT SAMPLE:

// The first argument is a file where each line of input contains a string with
// zero-based number.

// For example:

// 00 0 0 00 00 0
// 00 0
// 00 0 0 000 00 0000000 0 000
// 0 000000000 00 00

// OUTPUT SAMPLE:

// For each line of input, print an integer converted from a zero-based number.

// For example:

// 9
// 1
// 9208
// 3

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func juggle(l string) string {
	zeros := strings.Split(l, " ")
	s := []string{}
	for i := 0; i < len(zeros); i += 2 {
		a := zeros[i : i+2]
		flag, b := a[0], a[1]
		switch {
		case flag == "00":
			// append ones
			b = strings.Replace(b, "0", "1", -1)
			s = append(s, b)
		default:
			s = append(s, b)
		}
	}
	bits := strings.Join(s, "")
	i, err := strconv.ParseInt(bits, 2, 64)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing string to int", err.Error())
	}
	return fmt.Sprintf("%d", i)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(juggle(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
