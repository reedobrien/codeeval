package main

import "testing"

func TestWine(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"Cabernet Merlot Noir | ot", "Merlot"},
		{"Chardonnay Sauvignon | ann", "Chardonnay Sauvignon"},
		{"Shiraz Grenache | o", "False"},
		{"hdcsmwopogi juscqggtia crgbsg tlnelxaibqkuu kzgpq | fpjh", "False"},
		{"ebz twmudjccwl | weh", "False"},
		{"qwburoi imwnxxbho ziuwb cxutt ftveoke sxmdxn wjdkquuspwv gdu gp | aidl", "False"},
		{"gwuhdgafewpszh lhjl dlfkynxblzrs vkxbvgxdq lrdc shgznibiikrq rheddfgaaed nbwtsre xeguutri | dnqn", "False"},
		{"crtomifjbnb aurjafjdf bywyssesja sieaol yjhuwv | xfsu", "False"},
		{"qupkjes pmvohe vq ceqfbtklpdmzdzh subgy wobjwg lhcuazb gncqaiudrphwn | ljhiy", "False"},
		{"tnrtqdzbeovttam iceafcczmccfeyn rokfaapboanes bsz dx | zutqi", "False"},
		{"foycxrgpw gygnwrv | vtmid", "False"},
		{"ol yepwtwtndyegbzw kfawxcccecue | vds", "False"},
		{"egh veve srabm chwtmnejs pbgqgeo tzyhvnszw vshukqscqywj | ghxjs", "False"},
		{"voankeqy odpqfyegepp ypux ghwqmjj telifrvstwo elhmtphb wyizelvhcuqo cyzscyhnzqzgtea asllve | gs", "cyzscyhnzqzgtea"},
		{"ostiiduqmoqaay fwy qzxh hlryxbsxh tcvzqmxutprqp bfavvjvsrcppa | mur", "tcvzqmxutprqp"},
		{"otbgsbjskkphx qofbinnjablmzv kr yomn svdutwxrnbaqtcd gccnecwomfejmed | mtnsv", "False"},
		{"uyfjmljwioee tirfxcmfdwvmgk tgg nqxdnpo lnqrxl atfeo pjqjo | ywklb", "False"},
		{"gmhuhxysd pywagurihkkmf jlzannwfzyxnk ymu ejjlwgnvbdrfh db vjr syimhur eooqzzits | v", "ejjlwgnvbdrfh vjr"},
		{"jdeybofkjreg xgful vpwee pujjoqquaymd xrtowzve fhrsihuautpl oiaektrjui ye cnzibulksv | gju", "False"},
		{"rhmtrktselx gbqzo das jzrnfede ndig dgi | l", "rhmtrktselx"},
		{"x xaxx xaxax | xxxaa", "xaxax"},
		{"iizwhh ajoqplbyvp rwcmrekc msoucxdo fxjbujb uvqfzjjev auown tc ntskypidcldgwzk | fe", "uvqfzjjev"},
		{"pugzopnsw uwuzdpvkopi fybp wss pydtgarhpk zfqnpqzibdts | zbs", "zfqnpqzibdts"},
		{"ij olqgx pqfou izypx hqs iiofhf xxzrvevmcaxvs xlhxjdpi zguyzeqokmpjs ielhuyxtgjbyatb | tpthd", "False"},
		{"gczk zllnxm ate | vwffe", "False"},
		{"euqepif kie yzjyvws ckyrtjsfnef khqiwtscsexm xgdysfpfhkoclz otqrqlnzsha ewdnjsbs rtgprkpfdnbmxwm yfvfsjbe | jls", "False"},
		{"pqaahpzzx deptgcrolmgg twnh npno nijzffb qaskvxg | yy", "False"},
		{"eocgmirinkqai kpjuufwi qxvidofp ayutqt oxujeqepqyr rzilbtqydgnfx | ex", "oxujeqepqyr"},
		{"kitqfvqclfpqtt wvvfptrebblkj jnzlgmthcd vptsr pnikqqnzdyyehh egi xwkosn dfeai ggqlmrykhwzhdge | uun", "False"},
		{"clwmooync kpxxxh siglhdlthez nyabfbwbffxtf azatofubw | zzsmj", "False"},
		{"kfmqn ntoiilmwmglxgnw rsbbrjphl fofuqlyhuunm fhajboxxkqistk hnoqotuuuikbt wjct qynhlbrgb uafkvejmhixx vjobpfzwxpshvm | wnr", "False"},
		{"efjigflzmzxuzs eyvzghf snqqg hnkaojtvm hthhjucyhh lifbjeyhmp ycrkpfupos | m", "efjigflzmzxuzs hnkaojtvm lifbjeyhmp"},
		{"ddj cxuwemqe nb vipbcxnjcsfichm | io", "False"},
		{"srar frkcjafsj vzctrahrnh khugwnue qbjmpziy | fl", "False"},
		{"udibhtqchpkm iaufafcotcjrct | jqhbo", "False"},
		{"np fqgpvvsbrsm plzwevvjukt eoyjdukuhddvr ojhcjtbvcomci bdat | nwnv", "False"},
		{"nzg hiteywecqgm yboooi buicwjpeoxokjho paxgvmcdkeb ezvicbuxr ejneatgcfz prp | h", "hiteywecqgm buicwjpeoxokjho"},
		{"twzfuspzpupr dyecucw iagcat byaptaquuovlf wlyphlbky jxbeh hxogd vqtxjnunuldzzq | l", "byaptaquuovlf wlyphlbky vqtxjnunuldzzq"},
		{"jn vybmtvjhollaz tmbvsmsd xgieb wxoaom uxvitjujwcm aejeuavbojqudby tshlawkot qglcc ntfekfntzdj | q", "aejeuavbojqudby qglcc"},
		{"bypvjrjjufjgoz uk undabpg ragsmovbehamcah anjcuyup wqgrbng yzcmyegimsq | kf", "False"},
		{"zutwdjwq zc tdikdlqwbsqd wbgxpftyyto auh | b", "tdikdlqwbsqd wbgxpftyyto"},
		{"jammfjslz ohdovsaoojogqk cjotpr | loj", "False"},
		{"higuyhwk ycgp rukofzfoh wctarkh ttycknqgmgpmwv | qtmxi", "False"},
	}
	for _, test := range table {
		got := wineCheck(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %s, want %s\n", test.in, got, test.want)
		}
	}
}

func BenchmarkWines(b *testing.B) {
	for i := 0; i < b.N; i++ {
		wineCheck("eocgmirinkqai kpjuufwi qxvidofp ayutqt oxujeqepqyr rzilbtqydgnfx | ex")
	}
}
