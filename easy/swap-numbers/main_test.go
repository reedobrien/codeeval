package main

import "testing"

func TestCountArrows(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"4Always0 5look8 4on9 7the2 4bright8 9side7 3of8 5life5", "0Always4 8look5 9on4 2the7 8bright4 7side9 8of3 5life5"},
		{"5Nobody5 7expects3 5the4 6Spanish4 9inquisition0", "5Nobody5 3expects7 4the5 4Spanish6 0inquisition9"},
	}
	for _, test := range table {
		got := swap(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
