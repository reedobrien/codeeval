// Write a program which reverses the words in an input sentence.
//
// INPUT SAMPLE:
//
// The first argument is a file that contains multiple sentences, one per line.
// Empty lines are also possible.
//
// For example:
//
// Hello World
// Hello CodeEval
//
// OUTPUT SAMPLE:
//
// Print to stdout each sentence with the reversed words in it, one per line.
// Empty lines in the input should be ignored. Ensure that there are no
// trailing empty spaces in each line you print.
//
// For example:
//
// World Hello
// CodeEval Hello
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		entry := scanner.Text()
		fmt.Fprintln(os.Stdout, reverseWords(entry))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

}

func reverseWords(l string) string {
	var reversed []string
	words := strings.Split(l, " ")
	for i := len(words) - 1; i >= 0; i-- {
		reversed = append(reversed, words[i])
	}
	return strings.Join(reversed, " ")
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
