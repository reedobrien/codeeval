package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"64 6e 78 | 100101100 11110", "True"},
		{"5e 7d 59 | 1101100 10010101 1100111", "True"},
		{"93 75 | 1000111 1011010 1100010", "False"},
	}
	for _, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Fatalf("test %q got %q but wanted %q", test.in, got, test.want)
		}

	}
}
