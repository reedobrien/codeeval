// Given a number n and two integers p1,p2 determine if the bits in position p1 and p2 are the same or not. Positions p1 and p2 are 1 based.
//
// INPUT SAMPLE:
//
// The first argument will be a path to a filename containing a comma separated list of 3 integers, one list per line. E.g.
//
// 86,2,3
// 125,1,2
//
// OUTPUT SAMPLE:
//
// Print to stdout, 'true'(lowercase) if the bits are the same, else 'false'(lowercase). E.g.
//
// true
// false
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type entry struct {
	N      int
	P1, P2 uint
}

func (e entry) BitsEqual() bool {
	return ((e.N >> e.P1) & 1) == ((e.N >> e.P2) & 1)
}

func newEntry(l string) entry {
	eArr := strings.Split(l, ",")
	n, err := strconv.Atoi(eArr[0])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting N to int:", err)
		os.Exit(1)
	}
	p1, err := strconv.Atoi(eArr[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting P1 to int:", err)
		os.Exit(1)
	}
	p2, err := strconv.Atoi(eArr[2])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting P2 to int:", err)
		os.Exit(1)
	}
	return entry{N: n, P1: uint(p1 - 1), P2: uint(p2 - 1)}
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		entry := scanner.Text()
		e := newEntry(entry)
		fmt.Fprintln(os.Stdout, e.BitsEqual())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
