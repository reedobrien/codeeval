// Matrix Rotation

// Challenge Description:

// You are given a 2D N×N matrix. Each element of the matrix is a letter: from ‘a’ to ‘z’. Your task is to rotate the matrix 90° clockwise:

// a b c        g d a
// d e f  =>    h e b
// g h i        i f c

// Input sample:

// The first argument is a file that contains 2D N×N matrices, presented in a serialized form (starting from the upper-left element), one matrix per line. The elements of a matrix are separated by spaces.

// For example:

// a b c d
// a b c d e f g h i j k l m n o p
// a b c d e f g h i

// Output sample:

// Print to stdout matrices rotated 90° clockwise in a serialized form (same as in the input sample).

// For example:

// c a d b
// m i e a n j f b o k g c p l h d
// g d a h e b i f c

// Constraints:

//     The N size of the matrix can be from 1 to 10
//     The number of test cases is 100
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"strings"
)

func rot(s string) string {
	sel := []string{}
	rows := strings.Split(s, " ")
	// naive acquisition of n, truncates
	n := int(math.Sqrt(float64(len(rows))))
	m := [][]string{}
	for i := 0; i < n; i++ {
		// Create an appropriately sized 2d array so we can assign to the
		// positions without indexing out of bounds.
		row := []string{}
		row = append(row, rows[i*n:i*n+n]...)
		m = append(m, row)
	}
	for i := 0; i < n; i++ {
		// get the elements for the orig row
		sel = rows[i*n : i*n+n]
		for p := 0; p < n; p++ {
			// assign them to the new column
			m[p][n-i-1] = sel[p]
		}
	}
	out := []string{}
	for _, r := range m {
		out = append(out, r...)
	}
	return strings.Join(out, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(rot(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err.Error())
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
