package main

import "testing"

func TestVolume(t *testing.T) {
	table := []struct {
		in   []int
		want int
	}{
		{[]int{5, 3, 7, 2, 6, 4, 5, 9, 1, 2}, 14},
		{[]int{2, 6, 3, 5, 2, 8, 1, 4, 2, 2, 5, 3, 5, 7, 4, 1}, 35},
		{[]int{1, 5, 3, 7, 2}, 2},
		{[]int{3, 7, 7, 2}, 0},
		{[]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}, 0},
	}
	for _, test := range table {
		got := volume(test.in)
		if got != test.want {
			t.Fatalf("for %q got %q but want %q", test.in, got, test.want)
		}
	}
}
