package main

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestMove(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"g2", "e1 e3 f4 h4"},
		{"a1", "b3 c2"},
		{"d6", "b5 b7 c4 c8 e4 e8 f5 f7"},
		{"e5", "c4 c6 d3 d7 f3 f7 g4 g6"},
		{"b1", "a3 c3 d2"},
	}
	for _, tv := range values {
		equals(t, move(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Fprintf(os.Stderr, "\033[31m%s:%d:\n\n\t got: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
