// Details

// Challenge Description:

// There are two details on a M*N checkered field. The detail X covers several (at least one first cell) cells in each line. The detail Y covers several (at least one last cell) cells. Each cell is either fully covered with a detail or not.

// For example:

// Also, the details may have cavities (or other complex structures). Please see example below (the detail Y is one detail):

// The detail Y starts moving left (without any turn) until it bumps into the X detail at least with one cell. Determine by how many cells the detail Y will be moved.
// Input sample:

// The first argument is a file with different test cases. Each test case contains a matrix the lines of which are separated by comma. (Empty cells are marked as ".")

// For example:

// XX.YY,XXX.Y,X..YY,XX..Y
// XXX.YYYY,X...Y..Y,XX..YYYY,X.....YY,XX....YY
// XX...YY,X....YY,XX..YYY,X..YYYY
// XXYY,X..Y,XX.Y

// Output sample:

// 1
// 1
// 2
// 0

// Print out the number of cells the detail Y will be moved.

// For example:
// Constraints:

//     The matrices can be of different M*N sizes. (2 <= M <= 10, 2 <= N <= 10)
//     Number of test cases is 40.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

var (
	grid         = []string{}
	least, count int
	dist         = regexp.MustCompile(`X+\.+Y+`)
)

func distance(s string) string {
	grid = strings.Split(s, ",")
	least = len(grid)
	for _, r := range grid {
		count = strings.Count(dist.FindString(r), ".")
		if count < least {
			least = count
		}
	}
	return fmt.Sprintf("%d", least)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(distance(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input from file:", err.Error())
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input file:", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
