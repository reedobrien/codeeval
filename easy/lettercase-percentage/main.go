// LETTERCASE PERCENTAGE RATIO CHALLENGE DESCRIPTION:

// Your task is to write a program which determines (calculates) the percentage
// ratio of lowercase and uppercase letters.

// INPUT SAMPLE:

// Your program should accept a file as its first argument. Each line of input
// contains a string with uppercase and lowercase letters.

// For example:

// thisTHIS
// AAbbCCDDEE
// N
// UkJ

// OUTPUT SAMPLE:

// For each line of input, print the percentage ratio of uppercase and
// lowercase letters rounded to the second digit after the point.

// For example:

// lowercase: 50.00 uppercase: 50.00
// lowercase: 20.00 uppercase: 80.00
// lowercase: 0.00 uppercase: 100.00
// lowercase: 33.33 uppercase: 66.67

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"unicode"
)

func calc(l string) string {
	var lo, up float64
	a := []rune(l)
	for _, r := range a {
		switch {
		case unicode.IsUpper(r):
			up++
		case unicode.IsLower(r):
			lo++
		default:
			fmt.Fprintln(os.Stderr, "No upper or lower for rune:", r)
		}
	}
	ln := float64(len(a))
	return fmt.Sprintf("lowercase: %.02f uppercase: %.02f", lo/ln*100, up/ln*100)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(calc(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
