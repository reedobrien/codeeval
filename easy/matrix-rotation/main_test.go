package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestRot(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"a b c d", "c a d b"},
		{"a b c d e f g h i j k l m n o p", "m i e a n j f b o k g c p l h d"},
		{"a b c d e f g h i", "g d a h e b i f c"},
	}
	for _, tv := range values {
		equals(t, rot(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
