// Write a program which finds the next-to-last word in a string.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input example is the following
//
// some line with text
// another line
// Each line has more than one word.
//
// OUTPUT SAMPLE:
//
// Print the next-to-last word in the following way.
//
// with
// another
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func secondToLast(s string) string {
	words := strings.Split(s, " ")
	return words[len(words)-2]
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(secondToLast(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
