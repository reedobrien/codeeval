// You will be given a hexadecimal (base 16) number. Convert it into decimal (base 10).
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Each line in this file contains a hex number. You may assume that the hex number does not have the leading 'Ox'. Also all alpha characters (a through f) in the input will be in lowercase. E.g.
//
// 9f
// 11
//
// OUTPUT SAMPLE:
//
// Print out the equivalent decimal number. E.g.
//
// 159
// 17
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func hexToDec(s string) int {
	i, err := strconv.ParseInt(s, 16, 32)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error extracting int from hex string:", err)
	}
	return int(i)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Printf("%d\n", hexToDec(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
