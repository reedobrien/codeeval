// Clean up the words

// Challenge Description:

// You have a list of words. Letters of these words are mixed with extra
// symbols, so it is hard to define the beginning and end of each word. Write a
// program that will clean up the words from extra numbers and symbols.
// Input sample:

// (--9Hello----World...--)
// Can 0$9 ---you~
// 13What213are;11you-123+138doing7

// The first argument is a path to a file. Each line includes a test case with
// a list of words: letters are both lowercase and uppercase, and are mixed
// with extra symbols.

// For example:
// Output sample:

// hello world
// can you
// what are you doing

// Print the cleaned up words separated by spaces in lowercase letters.

// For example:
// Constraints:

//     Print the words separated by spaces in lowercase letters.  The length of
//     a test case together with extra symbols can be in a range from 10 to 100
//     symbols.
//     The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"
)

var numbers = []int{3, 7, 31, 127, 2047}

func cleanUp(s string) string {
	var results []rune
	var lastChar bool
	for _, r := range s {
		if unicode.IsLetter(r) {
			lastChar = true
			results = append(results, r)
			continue
		}
		if lastChar {
			results = append(results, ' ')
		}
		lastChar = false
	}
	return strings.ToLower(strings.TrimSpace(string(results)))
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(cleanUp(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
