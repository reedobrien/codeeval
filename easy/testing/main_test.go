package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"Heelo Codevval | Hello Codeeval", "Low"},
		{"hELLO cODEEVAL | Hello Codeeval", "Critical"},
		{"Hello Codeeval | Hello Codeeval", "Done"},
	}
	for i, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Errorf("test %d for %s got: %s wanted: %s", i, test.in, got, test.want)
		}
	}
}
