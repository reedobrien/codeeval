// An Armstrong number is an n-digit number that is equal to the sum of the
// n'th powers of its digits. Determine if the input numbers are Armstrong
// numbers.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Each
// line in this file has a positive integer. E.g.
//
// 6
// 153
// 351
//
// OUTPUT SAMPLE:
//
// Print out True/False if the number is an Armstrong number or not. E.g.
//
// True
// True
// False
package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

func isArmstrong(n string) string {
	var e float64
	want := float64(atoi(n))
	d := strings.Split(n, "")
	e = float64(len(d))
	var Σ float64
	for _, char := range d {
		Σ += math.Pow(float64(atoi(char)), e)
	}
	if Σ == want {
		return "True"
	}
	return "False"
}

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting numeral char to int:", err)
		os.Exit(1)
	}
	return i
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(isArmstrong(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
