// There is a game where each player picks a number from 1 to 9, writes it on a
// paper and gives to a guide. A player wins if his number is the lowest
// unique. We may have 10-20 players in our game.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename.
//
// You're a guide and you're given a set of numbers from players for the round
// of game. E.g. 2 rounds of the game look this way:
//
// 3 3 9 1 6 5 8 1 5 3
// 9 2 9 9 1 8 8 8 2 1 1
//
// OUTPUT SAMPLE:
//
// Print a winner's position or 0 in case there is no winner. In the first line of input sample the lowest unique number is 6. So player 5 wins.
//
// 5
// 0
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting input to int:", err)
		os.Exit(1)
	}
	return i
}
func lun(s string) int {
	var (
		niq      []int
		position int
	)
	digits := strings.Split(s, " ")
	count := map[int]int{}
	for _, d := range digits {
		count[atoi(d)]++
	}
	for k, v := range count {
		if 0 < v && v < 2 {
			niq = append(niq, k)
		}
	}
	if len(niq) == 0 {
		return position
	}
	sort.Ints(niq)
	winner := strconv.Itoa(niq[0])
	for i, d := range digits {
		if d == winner {
			position = i + 1
		}
	}
	return position
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(lun(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
