// FIND THE HIGHEST SCORE
// CHALLENGE DESCRIPTION:
//
// You decided to hold a banquet in honor of the World Art Day, where you
// invited all designers and artists that you know. During the banquet, you
// decided to find out which art movement and style the ordinary people like
// most of all, and whose works can get the highest score. To find the answer,
// you decided to hold an exhibition, where viewers will be able to evaluate
// each painting and vote for or against it. Each artist should create one work
// per each art movement.  After the exhibition, the participants calculated
// votes that they received for each painting and inserted them in the table.
// But, they could not determine which movement has won and whose work received
// the highest score, so they asked you to help.
//
// You need to determine and print the highest score of each category in the table.
//
// INPUT SAMPLE:
//
// The first argument is a path to a file. Each line includes a test case with a table. Table rows are separated by pipes '|'. All table rows contain scores for each category, so all lines are of an equal length.
//
// For example:
//
// 72 64 150 | 100 18 33 | 13 250 -6
// 10 25 -30 44 | 5 16 70 8 | 13 1 31 12
// 100 6 300 20 10 | 5 200 6 9 500 | 1 10 3 400 143
//
// OUTPUT SAMPLE:
//
// You need to print the highest score for each category.
//
// For example:
//
// 100 250 150
// 13 25 70 44
// 100 200 300 400 500
// CONSTRAINTS:

// All lines in a test case are of an equal length.
// The number of participants can be from 2 to 10 people.
// The number of categories can be from 4 to 20.
// The number of points for one picture can be from -1000 to 1000.
// The number of test cases is 40.
package main

import (
	"bufio"
	"container/heap"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// An maxHeap is a max-heap of ints.
// Modified from https://golang.org/pkg/container/heap/#example__intHeap
type maxHeap []int

func (h maxHeap) Len() int { return len(h) }

// i>j for max heap
func (h maxHeap) Less(i, j int) bool { return h[i] > h[j] }
func (h maxHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *maxHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *maxHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting input to int:", err)
		os.Exit(1)
	}
	return i
}

func initScoreboard(l int) []*maxHeap {
	var sb []*maxHeap
	for i := 0; i < l; i++ {
		h := &maxHeap{}
		heap.Init(h)
		sb = append(sb, h)
	}
	return sb
}

func highestScores(s string) string {
	scores := strings.Split(s, "|")
	for i := range scores {
		scores[i] = strings.TrimSpace(scores[i])

	}
	numGames := len(strings.Split(scores[0], " "))
	scoreboard := initScoreboard(numGames)
	for _, b := range scores {
		for i, s := range strings.Split(b, " ") {
			heap.Push(scoreboard[i], atoi(s))
		}
	}
	var results []string
	for _, g := range scoreboard {
		results = append(results, strconv.Itoa((*g)[0]))
	}
	return strings.Join(results, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(highestScores(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
