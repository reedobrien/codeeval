// TESTING
// CHALLENGE DESCRIPTION:
//
// In many teams, there is a person who tests a project, finds bugs and errors,
// and prioritizes them.
// Now, you have the unique opportunity to try yourself as a tester and test a
// product. Here, you have two strings - the first one is provided by
// developers, and the second one is mentioned in design. You have to find and
// count the number of bugs, and also prioritize them for fixing using the
// following statuses: 'Low', 'Medium', 'High', 'Critical' or 'Done'.
//
// INPUT SAMPLE:
// The first argument is a path to a file. Each line includes a test case with two strings separated by a pipeline '|'. The first string is the one the developers provided to you for testing, and the second one is from design.
//
// Heelo Codevval | Hello Codeeval
// hELLO cODEEVAL | Hello Codeeval
// Hello Codeeval | Hello Codeeval
//
// OUTPUT SAMPLE:
// Write a program that counts the number of bugs and prioritizes them for fixing using the following statuses:
//
// 'Low' - 2 or fewer bugs;
// 'Medium' - 4 or fewer bugs;
// 'High' - 6 or fewer bugs;
// 'Critical' - more than 6 bugs;
// 'Done' - all is done;
//
// Low
// Critical
// Done
//
// CONSTRAINTS:
// Strings are of the same length from 5 to 40 characters.
// Upper and lower case matters.
// The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func process(s string) string {
	var errCount int
	spl := strings.Split(s, " | ")
	impl, des := spl[0], spl[1]
	for i := 0; i < len(des); i++ {
		if impl[i] != des[i] {
			errCount++
		}
	}
	switch errCount {
	case 0:
		return "Done"
	case 1, 2:
		return "Low"
	case 3, 4:
		return "Medium"
	case 5, 6:
		return "High"
	default:
		return "Critical"
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		log.Fatalln("error opening file:", err)
	}
	procLines(f)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(process(line))
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln("error opening file:", err)
	}
}
