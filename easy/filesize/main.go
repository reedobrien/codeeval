// Print the size of a file in bytes.

// INPUT:

// The first argument to your program has the path to the file you need to
// check the size of.

// OUTPUT SAMPLE:

// Print the size of the file in bytes. E.g.

// 55

package main

import (
	"fmt"
	"os"
)

func main() {
	fpath := os.Args[1]
	f, err := os.Stat(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	fmt.Println(f.Size())
}
