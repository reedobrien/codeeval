// A number is a self-describing number when (assuming digit positions are
// labeled 0 to N-1), the digit in each position is equal to the number of
// times that that digit appears in the number.
//
// INPUT SAMPLE:
//
// The first argument is the pathname to a file which contains test data, one
// test case per line. Each line contains a positive integer. E.g.
//
// 2020
// 22
// 1210
//
// OUTPUT SAMPLE:
//
// If the number is a self-describing number, print out 1. If not, print out 0.
// E.g.
//
// 1
// 0
// 1
//
// For the curious, here's how 2020 is a self-describing number: Position '0'
// has value 2 and there is two 0 in the number. Position '1' has value 0
// because there are not 1's in the number. Position '2' has value 2 and there
// is two 2. And the position '3' has value 0 and there are zero 3's.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func strToNums(s string) []int {
	var nums []int
	digits := strings.Split(s, "")
	for _, d := range digits {
		num, err := strconv.Atoi(d)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting digit in file to int:", err)
		}
		nums = append(nums, num)
	}
	return nums
}

func isSelfDescribing(s string) string {
	count := make(map[int]int)
	digits := strToNums(s)
	for _, d := range digits {
		count[d]++
	}
	for i, d := range digits {
		if d != count[i] {
			return "0"
		}
	}
	return "1"
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Fprintln(os.Stdout, isSelfDescribing(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
