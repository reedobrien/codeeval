// SPLIT THE NUMBER
// CHALLENGE DESCRIPTION:

// You are given a number N and a pattern. The pattern consists of lowercase latin letters and one operation "+" or "-". The challenge is to split the number and evaluate it according to this pattern e.g.
// 1232 ab+cd -> a:1, b:2, c:3, d:2 -> 12+32 -> 44

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Each line of the file is a test case, containing the number and the pattern separated by a single whitespace. E.g.

// 3413289830 a-bcdefghij
// 776 a+bc
// 12345 a+bcde
// 1232 ab+cd
// 90602 a+bcde

// OUTPUT SAMPLE:

// For each test case print out the result of pattern evaluation. E.g.

// -413289827
// 83
// 2346
// 44
// 611

// Constraints:
// N is in range [100, 1000000000]
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func splitnum(l string) string {
	var (
		op = "+"
		fn []string
	)
	input := strings.Split(l, " ")
	if strings.Contains(input[1], "+") {
		fn = strings.Split(input[1], "+")
	} else {
		op = "-"
		fn = strings.Split(input[1], "-")
	}
	digits := strings.Split(input[0], "")
	a, b := strings.Join(digits[:len(fn[0])], ""), strings.Join(digits[len(fn[0]):], "")
	x, err := strconv.Atoi(a)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing str to int:", err)
		os.Exit(1)
	}
	y, err := strconv.Atoi(b)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing str to int:", err)
		os.Exit(1)
	}
	if op == "+" {
		return strconv.Itoa(x + y)
	} else {
		return strconv.Itoa(x - y)
	}
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(splitnum(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
