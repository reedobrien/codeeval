package main

import "sync"

func volume(towers []int) int {
	var (
		ltr = make([]int, len(towers))
		rtl = make([]int, len(towers))
		wg  sync.WaitGroup
	)

	wg.Add(2)

	go func() {
		max := 0
		for i := 0; i < len(towers); i++ {
			if towers[i] > max {
				max = towers[i]
			}
			ltr[i] = max
		}
		wg.Done()
	}()
	go func() {
		max := 0
		for i := len(towers) - 1; i >= 0; i-- {
			if towers[i] > max {
				max = towers[i]
			}
			rtl[i] = max
		}
		wg.Done()
	}()
	wg.Wait()
	var sum int
	for i := 0; i < len(towers); i++ {
		ltr[i] = min(ltr[i], rtl[i])
		sum = sum + (ltr[i] - towers[i])
	}
	return sum
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

/*
For a single processor, a 2 sweep (left->right, followed by a right->left
summation) is optimal, as many people have pointed out, but using many
processors, it is possible to complete this task in O(log n) time. There are
many ways to do this, so I'll explain one that is fairly close to the
sequential algorithm.

Max-cached tree O(log n)

1: Create a binary tree of all towers such that each node contains the height
of the highest tower in any of its children. Since the two leaves of any node
can be computed independently, this can be done in O(log n) time with n cpu's.

2a: Then, for each node in the tree, starting at the root, let the right leaf
have the value max(left, self, right). This will create the left-to-right
monotonic sweep in O(log n) time, using n cpu's.

2b: To compute the right-to-left sweep, we do the same procedure as before.
Starting with root of the max-cached tree, let the left leaf have the value
max(left, self, right). These left-to-right (2a) and right-to-left (2b) sweeps
can be done in parallel if you'd like to. They both use the max-cached tree as
input, and generate one new tree each (or sets their own fields in original
tree, if you prefer that).

3: Then, for each tower, the amount of water on it is min(ltr, rtl) -
towerHeight, where ltr is the value for that tower in the left-to-right
monotonic sweep we did before, i.e. the maximum height of any tower to the left
of us (including ourselves1), and rtl is the same for the right-to-left sweep.

4: Simply sum this up using a tree in O(log n) time using n cpu's, and we're
done.

1 If the current tower is taller than all towers to the left of us, or taller
than all towers to the the right of us, min(ltr, rtl) - towerHeight is zero.


*/
