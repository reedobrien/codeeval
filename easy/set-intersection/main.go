// You are given two sorted list of numbers (ascending order). The lists
// themselves are comma delimited and the two lists are semicolon delimited.
// Print out the intersection of these two sets.
//
// INPUT SAMPLE:
//
// File containing two lists of ascending order sorted integers, comma
// delimited, one per line. E.g.
//
// 1,2,3,4;4,5,6
// 20,21,22;45,46,47
// 7,8,9;8,9,10,11,12
//
// OUTPUT SAMPLE:
//
// Print out the ascending order sorted intersection of the two lists, one per line. Print empty new line in case the lists have no intersection. E.g.
// 4
//
// 8,9
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

type intersection struct {
	Left         []string
	members      map[string]bool
	Right        []string
	intersecting []string
}

func newIntersection(line string) intersection {
	sides := strings.Split(line, ";")
	l, r := strings.Split(sides[0], ","), strings.Split(sides[1], ",")
	return intersection{Left: l, Right: r, members: make(map[string]bool)}
}

func (i *intersection) FindIntersection() {
	for _, m := range i.Left {
		i.members[m] = true
	}
	for _, m := range i.Right {
		if i.members[m] {
			i.intersecting = append(i.intersecting, m)
		}
	}
}

func (i *intersection) String() string {
	i.FindIntersection()
	return strings.Join(i.intersecting, ",")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		i := newIntersection(line)
		fmt.Fprintln(os.Stdout, i.String())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
