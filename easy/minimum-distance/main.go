// Minimum Distance

// Challenge Description:

// Alice is looking for a sorority to join for her first year at Acme University. There is a street consisting entirely of sorority houses near the university, and some of her high school friends have already joined sorority houses on the street. (More than one of her friends may live at the same sorority house.)

// Alice wants to visit her friends frequently, and needs a program that will help her pick an optimal house to visit them from. Each sorority house has a street number that indicates its location on the street. The optimal location will minimize the sum of differences between the number of Alice's house and the number of her friends' houses.

// For example: Alice's friends live at houses 3, 3, 5, and 7. Alice moves in at house 4. Then the distances to her friends' houses are 1, 1, 1, and 3, totaling 6.
// Input sample:

// The input consists of several integers on a line, separated by spaces. The first integer F contains the number of friends (0 < F < 100). Then F street addresses A follow (0 < A < 10000).

// For example:

// 4 3 3 5 7
// 3 20 30 40

// Output sample:

// Print a line containing the minimal sum of distances for an optimal sorority location.

// For example:

// 6
// 20

// Constraints:

//     Number of friends: 0 < F < 100
//     Street addresses: 0 < A < 10000
//     Number of test cases is 10.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

var (
	min, max, friends int
	addrs             []int
	distances         []int
)

func distance(s string) string {
	friends, addrs = procArgs(s)
	min, max = addrs[0], addrs[friends-1]
	distances = []int{}
	for i := min; i < max+1; i++ {
		distances = append(distances, getDistanceSum(i))
	}
	sort.Ints(distances)
	return strconv.Itoa(distances[0])
}

func getDistanceSum(d int) int {
	var sum float64
	for _, a := range addrs {
		sum += math.Abs(float64(d - a))
	}
	return int(sum)
}

func procArgs(s string) (int, []int) {
	digits := strings.Split(s, " ")
	ints := []int{}
	for _, d := range digits[1:] {
		i, err := strconv.Atoi(d)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error parsing int from string:", err.Error())
		}
		ints = append(ints, i)
	}
	ints = ints
	sort.Ints(ints)
	return len(ints), ints
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(distance(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input from file:", err.Error())
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input file:", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
