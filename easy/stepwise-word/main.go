// Stepwise word

// Challenge Description:

// Print the longest word in a stepwise manner.
// Input sample:

// cat dog hello
// stop football play
// music is my life

// The first argument is a path to a file. Each line contains a test case with
// a list of words that have different or the same length.

// For example:
// Output sample:

// h *e **l ***l ****o
// f *o **o ***t ****b *****a ******l *******l
// m *u **s ***i ****c

// Find the longest word in each line and print it in one line in a stepwise
// manner. Separate each new step with a space. If there are several words of
// the same length and they are the longest, then print the first word from the
// list.

// Constraints:
//     The word length is from 1 to 10 characters.
//     The number of words in a line is from 5 to 15.
//     If there are several words of the same length and they are the longest,
//     then print the first word from the list.
//     The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func stepwiseWord(s string) string {
	var (
		word, output string
		length       int
	)
	words := strings.Split(s, " ")
	for _, v := range words {
		if len(v) > length {
			word, length = v, len(v)
		}
	}
	for i, r := range word {
		format := fmt.Sprintf("%s%%c ", strings.Repeat("*", i))
		output = output + fmt.Sprintf(format, r)
	}
	return strings.TrimSpace(output)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(stepwiseWord(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
