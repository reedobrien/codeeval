// Given a positive integer, find the sum of its constituent digits.
//
// INPUT SAMPLE:
//
// The first argument will be a path to a filename containing positive
// integers, one per line. E.g.
//
// 23
// 496
//
// OUTPUT SAMPLE:
//
// Print to stdout, the sum of the numbers that make up the integer, one per
// line. E.g.
//
// 5
// 19
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

var sum int

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		sum = sumLine(scanner.Text())
		fmt.Fprintln(os.Stdout, sum)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

}

func sumLine(l string) int {
	digits := strings.Split(l, "")
	var Σ int
	for _, d := range digits {
		i, err := strconv.Atoi(d)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting digit str to int:", err)
			os.Exit(1)
		}
		Σ += i
	}
	return Σ
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
