// Longest Lines

// Challenge Description:

// Write a program which reads a file and prints to stdout the specified number of the longest lines that are sorted based on their length in descending order.
// Input sample:

// Your program should accept a path to a file as its first argument. The file contains multiple lines. The first line indicates the number of lines you should output, the other lines are of different length and are presented randomly. You may assume that the input file is formatted correctly and the number in the first line is a valid positive integer.

// For example:
// Output sample:

// San Francisco
// Hello World

// Print out the longest lines limited by specified number and sorted by their length in descending order.

// For example:
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type byLen []string

func (l byLen) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l byLen) Less(i, j int) bool { return len(l[i]) > len(l[j]) }
func (l byLen) Len() int           { return len(l) }

func longLines(lines []string) string {
	num, err := strconv.Atoi(lines[0])
	if err != nil {
		log.Fatalf("couldn't coerce %s to int\n", lines[0])
	}

	lines = lines[1:]
	sort.Sort(byLen(lines))
	return strings.Join(lines[:num], "\n")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	var lines []string
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	fmt.Println(longLines(lines))
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
