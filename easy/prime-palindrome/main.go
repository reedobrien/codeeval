// Write a program which determines the largest prime palindrome less than
// 1000.
//
// INPUT SAMPLE:
// There is no input for this program.
//
// OUTPUT SAMPLE:
// Print to stdout the largest prime palindrome less than 1000.
//
// 929
package main

import (
	"fmt"
	"math/big"
	"os"
	"strconv"
)

var (
	max     int = 1000
	largest int
)

func strReverse(s string) string {
	runes := []rune(s)
	var rev []rune
	for i := len(s) - 1; i >= 0; i-- {
		rev = append(rev, runes[i])
	}
	return string(rev)
}

func main() {
	for i := max; i > 0; i-- {
		if big.NewInt(int64(i)).ProbablyPrime(20) {
			pStr := strconv.Itoa(i)
			if pStr == strReverse(pStr) {
				fmt.Println(i)
				os.Exit(0)
			}
		}
	}
}
