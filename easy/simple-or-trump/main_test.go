package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"AD 2H | H", "2H"},
		{"KD KH | C", "KD KH"},
		{"JH 10S | C", "JH"},
		{"8C 10H | H", "10H"},
	}
	for _, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Fatalf("testing %q got %q but wanted %q", test.in, got, test.want)
		}
	}
}
