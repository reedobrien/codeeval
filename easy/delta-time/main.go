// DELTA TIME
// CHALLENGE DESCRIPTION:

// You are given the pairs of time values. The values are in the HH:MM:SS format with leading zeros. Your task is to find out the time difference between the pairs.

// INPUT SAMPLE:

// The first argument is a file that contains lines with the time pairs.

// For example:

// 14:01:57 12:47:11
// 13:09:42 22:16:15
// 08:08:06 08:38:28
// 23:35:07 02:49:59
// 14:31:45 14:46:56

// OUTPUT SAMPLE:

// Print to stdout the time difference for each pair, one per line. You must format the time values in HH:MM:SS with leading zeros.

// For example:

// 01:14:46
// 09:06:33
// 00:30:22
// 20:45:08
// 00:15:11

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

var format = "15:04:05"

func delta(l string) string {
	a := strings.Split(l, " ")
	t1, err := time.Parse(format, a[0])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing time:", err.Error())
	}
	t2, err := time.Parse(format, a[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing time:", err.Error())
	}
	var dur time.Duration
	if t1.Before(t2) {
		dur = t2.Sub(t1)
	} else {
		dur = t1.Sub(t2)
	}
	h := int(dur.Hours())
	m := int(dur.Minutes() - float64(h*60))
	s := int(dur.Seconds() - float64((m*60 + (h * 60 * 60))))

	return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(delta(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
