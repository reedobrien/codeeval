package main

import "testing"

func TestCountArrows(t *testing.T) {
	table := []struct {
		in   []string
		want string
	}{
		{[]string{"2", "Hello World", "CodeEval", "Quick Fox", "A", "San Francisco"}, "San Francisco\nHello World"},
	}
	for _, test := range table {
		got := longLines(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
