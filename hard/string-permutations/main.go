// STRING PERMUTATIONS

// CHALLENGE DESCRIPTION:

// Write a program which prints all the permutations of a string in alphabetical order. We consider that digits < upper case letters < lower case letters. The sorting should be performed in ascending order.

// INPUT SAMPLE:

// Your program should accept a file as its first argument. The file contains input strings, one per line.
// For example:

// hat
// abc
// Zu6

// OUTPUT SAMPLE:

// Print to stdout the permutations of the string separated by comma, in alphabetical order.
// For example:

// aht,ath,hat,hta,tah,tha
// abc,acb,bac,bca,cab,cba
// 6Zu,6uZ,Z6u,Zu6,u6Z,uZ6
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

func permutation(s string, start int, p *[]string) string {
	var out string
	ln := len(s)
	if ln == start {
		*p = append(*p, s)
	} else {
		permutation(s, start+1, p)
	}
	for i := start + 1; i < ln; i++ {
		in := []byte(s)
		in[start], in[i] = in[i], in[start]
		out = string(in)
		permutation(out, start+1, p)
	}
	sort.Strings(*p)
	return strings.Join(*p, ",")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		permutations := &[]string{}
		fmt.Println(permutation(line, 0, permutations))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
