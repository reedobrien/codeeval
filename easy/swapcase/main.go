//Write a program which swaps letters' case in a sentence. All non-letter
// characters should remain the same.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input
// example is the following
//
// Hello world!
// JavaScript language 1.8
// A letter
//
// OUTPUT SAMPLE:
//
// Print results in the following way.
//
// hELLO WORLD!
// jAVAsCRIPT LANGUAGE 1.8
// a LETTER
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"unicode"
)

func swapcase(s string) string {
	chars := []rune(s)
	for i, c := range chars {
		if unicode.IsLower(c) {
			chars[i] = unicode.ToUpper(c)
		} else {
			chars[i] = unicode.ToLower(c)
		}
	}
	return string(chars)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Fprintln(os.Stdout, swapcase(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
