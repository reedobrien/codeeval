// MERSENNE PRIME
// CHALLENGE DESCRIPTION:
//
// In January 2013, GIMPS contributors discovered the 48th known Mersenne prime
// number. For this, they received a $100,000 award. A newly announced $150,000
// reward will be given to those who will discover the next largest known prime// .
// Do not want to get it? We offer this challenge to you to get ready.
//
// INPUT SAMPLE:
//
// The first argument is a path to a file. Each line includes a test case with
// an integer.
//
// 4
// 308
// OUTPUT SAMPLE:
//
// Your task is to print all Mersenne numbers that are smaller than the number
// in a test case. Separate those numbers by commas.
//
// 3
// 3, 7, 31, 127
// CONSTRAINTS:
//
// The number in a test case can be from 4 to 3,000.
// The number of test cases is 20.
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

var numbers = []int{3, 7, 31, 127, 2047}

func mersenne(s string) string {
	results := []string{}
	t, err := strconv.Atoi(s)
	if err != nil {
		log.Fatalf("%s\n", err)
	}
	for _, n := range numbers {
		if n < t {
			results = append(results, strconv.Itoa(n))
		}
	}
	return strings.Join(results, ", ")
}

// func mersenne(s string) string {
// 	println(big.NewInt(int64(2047)).ProbablyPrime(3000))
// 	num, err := strconv.ParseInt(s, 10, 64)
// 	if err != nil {
// 		log.Fatalf("%s\n", err)
// 	}
// 	var (
// 		m   []string
// 		i   float64
// 		two = float64(2)
// 	)
// 	num = int64(num)
// 	for i = 2; ; i++ {
// 		t := int64(math.Pow(two, i) - 1)
// 		if t > num {
// 			break
// 		}

// 		if big.NewInt(int64(t)).ProbablyPrime(20) {
// 			m = append(m, strconv.Itoa(int(t)))
// 		}
// 	}
// 	return strings.Join(m, ", ")
// }

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(mersenne(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
