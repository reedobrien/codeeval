// JSON MENU IDS
// CHALLENGE DESCRIPTION:

// You have JSON string which describes a menu. Calculate the SUM of IDs of all
// "items" in the case a "label" exists for an item.

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Input
// example is the following

// {"menu": {"header": "menu", "items": [{"id": 27}, {"id": 0, "label": "Label 0"}, null, {"id": 93}, {"id": 85}, {"id": 54}, null, {"id": 46, "label": "Label 46"}]}}
// {"menu": {"header": "menu", "items": [{"id": 81}]}}
// {"menu": {"header": "menu", "items": [{"id": 70, "label": "Label 70"}, {"id": 85, "label": "Label 85"}, {"id": 93, "label": "Label 93"}, {"id": 2}]}}

// All IDs are integers between 0 and 100. It can be 10 items maximum for a
// menu.

// OUTPUT SAMPLE:

// Print results in the following way.

// 46
// 0
// 248
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
)

type lsum struct {
	value int
}

func (l *lsum) Add(n int) {
	l.value += n
}

func (l *lsum) String() string {
	return strconv.Itoa(l.value)
}

func layout(s string) string {
	Σ := lsum{0}
	c := &container{}
	err := json.Unmarshal([]byte(s), c)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing json string: ", err)
		os.Exit(1)
	}
	for _, item := range c.Menu.Items {
		if len(item.Label) > 0 {
			Σ.Add(item.ID)
		}
	}
	return Σ.String()
}

type container struct {
	Menu struct {
		Header string `json:"header"`
		Items  []struct {
			ID    int    `json:"id"`
			Label string `json:"label"`
		} `json:"items"`
	} `json:"menu"`
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(layout(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
