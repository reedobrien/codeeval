// SLANG FLAVOR
// CHALLENGE DESCRIPTION:

// Long serious texts are boring. Write a program that will make texts more informal: replace every other end punctuation mark (period ‘.’, exclamation mark ‘!’, or question mark ‘?’) with one of the following slang phrases, selecting them one after another:

// ‘, yeah!’
// ‘, this is crazy, I tell ya.’
// ‘, can U believe this?’
// ‘, eh?’
// ‘, aw yea.’
// ‘, yo.’
// ‘? No way!’
// ‘. Awesome!’
// The result should be funny.

// INPUT SAMPLE:

// The first argument is a file that contains a text.

// For example:

// Lorem ipsum dolor sit amet. Mea et habeo doming praesent. Te inani utroque recteque has, sea ne fugit verterem!
// Usu ei scripta phaedrum, an sed salutatus definiebas? Qui ut recteque gloriatur reformidans. Qui solum aeque sapientem cu.
// Eu nam nusquam quaestio principes.

// OUTPUT SAMPLE:

// Print to stdout the results: the text with slang phrases.

// For example:

// Lorem ipsum dolor sit amet. Mea et habeo doming praesent, yeah! Te inani utroque recteque has, sea ne fugit verterem!
// Usu ei scripta phaedrum, an sed salutatus definiebas, this is crazy, I tell ya. Qui ut recteque gloriatur reformidans. Qui solum aeque sapientem cu, can U believe this?
// Eu nam nusquam quaestio principes.

// CONSTRAINTS:

// In the input text, end punctuation mark cannot come one after another (consequent).
// Input text contains 40 lines.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
)

var flavors = []string{
	", yeah!",
	", this is crazy, I tell ya.",
	", can U believe this?",
	", eh?",
	", aw yea.",
	", yo.",
	"? No way!",
	". Awesome!",
}

type pos int

func (v *pos) next() {
	if int(*v)+1 >= len(flavors) {
		*v = 0
	} else {
		*v += 1
	}
}

var (
	p    pos
	even int8
)

func spice(s string) string {
	out := []rune{}
	for _, r := range s {
		switch r {
		case '.', '?', '!':
			even += 1
			if even%2 == 0 {
				out = append(out, []rune(flavors[p])...)
				p.next()
			} else {
				out = append(out, r)
			}
		default:
			out = append(out, r)
		}
	}
	return string(out)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(spice(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
