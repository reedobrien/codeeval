package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestExperience(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"Feb 2004-Dec 2009; Sep 2004-Jul 2008", "5"},
		{"Aug 2013-Mar 2014; Apr 2013-Aug 2013; Jun 2014-Aug 2015; Apr 2003-Nov 2004; Apr 2014-Jan 2015", "4"},
		{"Mar 2003-Jul 2003; Nov 2003-Jan 2004; Apr 1999-Nov 1999", "1"},
		{"Apr 1992-Dec 1993; Feb 1996-Sep 1997; Jan 2002-Jun 2002; Sep 2003-Apr 2004; Feb 2010-Nov 2011", "6"},
		{"Feb 2004-May 2004; Jun 2004-Jul 2004", "0"},
	}
	for _, tv := range values {
		equals(t, calc(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
