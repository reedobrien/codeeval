package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestSquash(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"But as he spake he drew the good sword from its scabbard, and smote a heathen knight, Jusssstin of thee Iron Valley.",
			"But as he spake he drew the god sword from its scabard, and smote a heathen knight, Justin of the Iron Valey."},
		{"No matttter whom you choose, she deccccclared, I will abide by your decision.",
			"No mater whom you chose, she declared, I wil abide by your decision."},
		{"Wwwhat is your will?", "Wwhat is your wil?"},
		{"At his magic speech the ground oppened and he began the path of descent.",
			"At his magic spech the ground opened and he began the path of descent."},
		{"I should fly away and you would never see me again.", "I should fly away and you would never se me again."},
	}
	for _, tv := range values {
		equals(t, squash(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
