// Knight Moves

// Challenge Description:

// In chess, the knight moves to any of the closest squares that are not on the same rank, file, or diagonal. Thus the move is in the “L” form: two squares vertically and one square horizontally, or two squares horizontally and one square vertically:

// Your task is to find all possible positions for the next move of the knight on the empty chessboard.
// Input sample:

// The first argument is a filename that contains positions of the knight on the chessboard in the CN form, where:

//     C is a letter from “a” to “h” and denotes a column.
//     N is a number from 1 to 8 and denotes a row.

// Each position is indicated in a new line.

// For example:

// g2
// a1
// d6
// e5
// b1

// Output sample:

// e1 e3 f4 h4
// b3 c2
// b5 b7 c4 c8 e4 e8 f5 f7
// c4 c6 d3 d7 f3 f7 g4 g6
// a3 c3 d2

// Print to stdout all possible positions for the next move of the knight ordered alphabetically.

// For example:
// Constraints:

//     The number of test cases is 40.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

var letters = map[int]string{
	1: "a",
	2: "b",
	3: "c",
	4: "d",
	5: "e",
	6: "f",
	7: "g",
	8: "h",
}

var c2l = "abcdefgh"

var moves = [][]int{{-2, -1}, {-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}}

type position struct {
	H, V int
}

func newPos(s string) position {
	c := strings.Split(s, "")
	h := strings.Index(c2l, c[0]) + 1
	v, err := strconv.Atoi(c[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting string to int", err.Error())
	}
	return position{h, v}
}

func move(s string) string {
	board := makeBoard()
	start := newPos(s)
	out := []string{}
	for _, m := range moves {
		dest := position{start.H + m[0], start.V + m[1]}
		_, ok := board[dest]
		if ok {
			out = append(out, fmt.Sprintf("%s%d", letters[dest.H], dest.V))
		}
	}
	sort.Strings(out)
	return strings.Join(out, " ")
}

func makeBoard() map[position]bool {
	board := make(map[position]bool, 64)
	for h := 1; h < 9; h++ {
		for v := 1; v < 9; v++ {
			board[position{h, v}] = false
		}
	}
	return board
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(move(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input from file:", err.Error())
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input file:", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
