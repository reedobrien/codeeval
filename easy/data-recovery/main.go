// DATA RECOVERY
// CHALLENGE DESCRIPTION:

// Your friends decided to make fun of you. They have installed a script on
// your computer which shuffled all words within a text. It is a joke, so they
// have left hints for each sentence. The hints will allow you to rebuild the
// data easily, but you need to find out how to use them.

// Your task is to write a program which reconstructs each sentence out of a
// set of words and prints out the original sentences.

// INPUT SAMPLE:

// Your program should accept a path to a filename as its first argument. Each
// line is a test case which consists of a set of words and a sequence of
// numbers separated by a semicolon. The words within a set and the numbers
// within a sequence are separated by a single space.

// For example:

// 2000 and was not However, implemented 1998 it until;9 8 3 4 1 5 7 2
// programming first The language;3 2 1
// programs Manchester The written ran Mark 1952 1 in Autocode from;6 2 1 7 5 3 11 4 8 9

// OUTPUT SAMPLE:

// Print out a reconstructed sentence for each test case, one per line.

// For example:

// However, it was not implemented until 1998 and 2000
// The first programming language
// The Manchester Mark 1 ran programs written in Autocode from 1952

// CONSTRAINTS:

// The number of test cases is in a range from 20 to 40.  The words consist of
// ASCII uppercase and lowercase letters, digits, and punctuation marks.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func recov(l string) string {
	m := makeMap(l)
	sentence := make([]string, len(m))
	for i := 0; i < len(m); i++ {
		sentence[i] = m[i+1]
	}
	return strings.Join(sentence, " ")
}

func makeMap(s string) map[int]string {
	splt := strings.Split(s, ";")
	line, order := splt[0], splt[1]
	positions := convertOrder(order)
	words := strings.Split(line, " ")
	var total int
	for i, _ := range words {
		total += i + 1
	}
	missing := total - sum(positions)
	m := map[int]string{}
	for i, n := range positions {
		m[n] = words[i]
	}
	m[missing] = words[len(words)-1]
	return m
}

func sum(a []int) int {
	var t int
	for _, n := range a {
		t += n
	}
	return t
}

func convertOrder(s string) []int {
	var a = []int{}
	for _, n := range strings.Split(s, " ") {
		i, err := strconv.Atoi(n)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error converting string to int: ", err.Error())
		}
		a = append(a, i)
	}
	return a
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(recov(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
