package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"1 8", "3"},
		{"2 4", "1"},
	}
	for i, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Errorf("test %d for %q got: %q wanted: %q", i, test.in, got, test.want)
		}
	}
}
