package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestCompress(t *testing.T) {

	values := []struct {
		in, want string
	}{
		{"40 40 40 40 29 29 29 29 29 29 29 29 57 57 92 92 92 92 92 86 86 86 86 86 86 86 86 86 86", "4 40 8 29 2 57 5 92 10 86"},
		{"73 73 73 73 41 41 41 41 41 41 41 41 41 41", "4 73 10 41"},
		{"1 1 3 3 3 2 2 2 2 14 14 14 11 11 11 2", "2 1 3 3 4 2 3 14 3 11 1 2"},
		{"7", "1 7"},
	}
	for _, tv := range values {
		equals(t, compress(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
