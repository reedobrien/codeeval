// Swap Numbers

// Challenge Description:

// Write a program that, given a sentence where each word has a single digit
// positive integer as a prefix and suffix, swaps the numbers while retaining
// the word in between. Words in the sentence are delimited from each other by
// a space.
// Input sample:

// 4Always0 5look8 4on9 7the2 4bright8 9side7 3of8 5life5
// 5Nobody5 7expects3 5the4 6Spanish4 9inquisition0

// The first argument is a path to a file. Each line of the input file contains
// one test case represented by a sentence. Each word in the sentence begins
// and ends with a single digit positive integer i.e. 0 through 9. Assume all
// characters are ASCII.
// Output sample:

// 0Always4 8look5 9on4 2the7 8bright4 7side9 8of3 5life5
// 5Nobody5 3expects7 4the5 4Spanish6 0inquisition9

// For each test case, print to standard output the sentence obtained by
// swapping the numbers surrounding each word, one per line.
// Constraints:

//     The suffix and the prefix of each word may be equal.
//     Sentences are form 1 to 17 words long.
//     The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func swap(s string) string {
	words := strings.Split(s, " ")
	for i, v := range words {
		a := strings.Split(v, "")
		a[0], a[len(a)-1] = a[len(a)-1], a[0]
		words[i] = strings.Join(a, "")
	}
	return strings.Join(words, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(swap(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
