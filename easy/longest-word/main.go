// LONGEST WORD
// CHALLENGE DESCRIPTION:
//
// In this challenge you need to find the longest word in a sentence. If the
// sentence has more than one word of the same length you should pick the first
// one.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input
// example is the following
//
// some line with text
// another line
// Each line has one or more words. Each word is separated by space char.
//
// OUTPUT SAMPLE:
//
// Print the longest word in the following way.
//
// some
// another
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func longestWord(s string) string {
	var longest, max int
	words := strings.Split(s, " ")
	for i, w := range words {
		if len(w) > max {
			max = len(w)
			longest = i
		}
	}
	return words[longest]
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(longestWord(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
