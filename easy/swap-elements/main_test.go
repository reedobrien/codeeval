package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestSwap(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"1 2 3 4 5 6 7 8 9 : 0-8", "9 2 3 4 5 6 7 8 1"},
		{"1 2 3 4 5 6 7 8 9 10 : 0-1, 1-3", "2 4 3 1 5 6 7 8 9 10"},
	}
	for _, tv := range values {
		equals(t, swap(tv.in), tv.want)
	}
}

func TestGetInput(t *testing.T) {
	values := []struct {
		in        string
		wantList  []string
		wantSwaps [][]int
	}{
		{"1 2 3 4 5 6 7 8 9 : 0-8", []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}, [][]int{[]int{0, 8}}},
		{"1 2 3 4 5 6 7 8 9 10 : 0-1, 1-3", []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, [][]int{[]int{0, 1}, []int{1, 3}}},
	}
	for _, tv := range values {
		l, s := getInput(tv.in)
		equals(t, l, tv.wantList)
		equals(t, s, tv.wantSwaps)
	}
}

// equals fails the test if got is not equal to want.
func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot:  %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
