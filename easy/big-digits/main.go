// BIG DIGITS
// CHALLENGE DESCRIPTION:

// In this challenge you're presented with a situation in which you need to
// output big symbols on devices which only support ASCII characters and
// single, fixed-width fonts. To do this you're going to use pseudo-graphics to
// ‘draw’ these big symbols.

// Here is an example of the font with digits from 0 to 9:

// -**----*--***--***---*---****--**--****--**---**--
// *--*--**-----*----*-*--*-*----*-------*-*--*-*--*-
// *--*---*---**---**--****-***--***----*---**---***-
// *--*---*--*-------*----*----*-*--*--*---*--*----*-
// -**---***-****-***-----*-***---**---*----**---**--
// --------------------------------------------------

// Each pixel is marked either with asterisk ‘*’ or with minus ‘-’. Size of a
// digit is 5×6 pixels.

// Your task is to write a program, which outputs the numbers given to you with
// the font as in the example.

// INPUT SAMPLE:

// The first argument is a file that contains the lines with digits sequences
// you need to magnify. E.g.:

// 3.1415926
// 1.41421356
// 01-01-1970
// 2.7182818284
// 4 8 15 16 23 42

// OUTPUT SAMPLE:

// Print to stdout the magnified digits:

// ***----*---*-----*--****--**--***---**--
// ---*--**--*--*--**--*----*--*----*-*----
// -**----*--****---*--***---***--**--***--
// ---*---*-----*---*-----*----*-*----*--*-
// ***---***----*--***-***---**--****--**--
// ----------------------------------------
// --*---*-----*---*---***----*--***--****--**--
// -**--*--*--**--*--*----*--**-----*-*----*----
// --*--****---*--****--**----*---**--***--***--
// --*-----*---*-----*-*------*-----*----*-*--*-
// -***----*--***----*-****--***-***--***---**--
// ---------------------------------------------
// -**----*---**----*----*---**--****--**--
// *--*--**--*--*--**---**--*--*----*-*--*-
// *--*---*--*--*---*----*---***---*--*--*-
// *--*---*--*--*---*----*-----*--*---*--*-
// -**---***--**---***--***--**---*----**--
// ----------------------------------------
// ***--****---*---**--***---**----*---**--***---**---*---
// ---*----*--**--*--*----*-*--*--**--*--*----*-*--*-*--*-
// -**----*----*---**---**---**----*---**---**---**--****-
// *-----*-----*--*--*-*----*--*---*--*--*-*----*--*----*-
// ****--*----***--**--****--**---***--**--****--**-----*-
// -------------------------------------------------------
// -*----**----*--****---*---**--***--***---*---***--
// *--*-*--*--**--*-----**--*-------*----*-*--*----*-
// ****--**----*--***----*--***---**---**--****--**--
// ---*-*--*---*-----*---*--*--*-*-------*----*-*----
// ---*--**---***-***---***--**--****-***-----*-****-
// --------------------------------------------------

// CONSTRAINTS:

// Input lines are up to 16 symbols long.  Input can contain some other
// symbols, which should be ignored (i.e. points, hyphens, spaces); only
// numbers must be printed out.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"
)

var digits = map[string][]string{
	"0": []string{"-**--", "*--*-", "*--*-", "*--*-", "-**--", "-----"},
	"1": []string{"--*--", "-**--", "--*--", "--*--", "-***-", "-----"},
	"2": []string{"***--", "---*-", "-**--", "*----", "****-", "-----"},
	"3": []string{"***--", "---*-", "-**--", "---*-", "***--", "-----"},
	"4": []string{"-*---", "*--*-", "****-", "---*-", "---*-", "-----"},
	"5": []string{"****-", "*----", "***--", "---*-", "***--", "-----"},
	"6": []string{"-**--", "*----", "***--", "*--*-", "-**--", "-----"},
	"7": []string{"****-", "---*-", "--*--", "-*---", "-*---", "-----"},
	"8": []string{"-**--", "*--*-", "-**--", "*--*-", "-**--", "-----"},
	"9": []string{"-**--", "*--*-", "-***-", "---*-", "-**--", "-----"},
}

func embiggen(l string) string {
	input := []rune(l)
	output := []string{}
	nums := []string{}
	for _, r := range input {
		if unicode.IsDigit(r) {
			nums = append(nums, string(r))
		}
	}
	for line := 0; line < 6; line++ {
		for _, d := range nums {
			output = append(output, digits[d][line])
		}
		output = append(output, "\n")
	}
	return strings.TrimSpace(strings.Join(output, ""))
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(embiggen(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
