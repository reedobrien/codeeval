package main

import "testing"

func TestMersanne(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"0", ""},
		{"4", "3"},
		{"308", "3, 7, 31, 127"},
		{"3000", "3, 7, 31, 127, 2047"},
	}
	for _, test := range table {
		got := mersenne(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %s, want %s\n", test.in, got, test.want)
		}
	}
}
