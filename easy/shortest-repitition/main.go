package main // SHORTEST REPETITION
import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

// CHALLENGE DESCRIPTION:

// Write a program to determine the shortest repetition in a string.  A string
// is said to have period p if it can be formed by concatenating one or more
// repetitions of another string of length p. For example, the string
// "xyzxyzxyzxyz" has period 3, since it is formed by 4 repetitions of the
// string "xyz". It also has periods 6 (two repetitions of "xyzxyz") and 12
// (one repetition of "xyzxyzxyzxyz").

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Each
// line will contain a string of up to 80 non-blank characters. E.g.

// abcabcabcabc
// bcbcbcbcbcbcbcbcbcbcbcbcbcbc
// dddddddddddddddddddd
// adcdefg

// OUTPUT SAMPLE:

// Print out the smallest period of the input string. E.g.

// 3
// 2
// 1
// 7

func shortestRep(line string) int {
	var i, p int
	runes := []rune(line)
	ln := len(runes)
	for _ = range line {
		i++
		if ln%i != 0 {
			continue
		}
		if strings.Repeat(string(runes[0:i]), ln/i) == line {
			p = i
			break
		}

	}
	return p
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(shortestRep(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
