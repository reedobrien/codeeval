// Having a string representation of a set of numbers you need to print this
// numbers.
//
// All numbers are separated by semicolon. There are up to 20 numbers in one
// line. The numbers are "zero" to "nine"
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Each
// line in this file is one test case. E.g.
//
// zero;two;five;seven;eight;four
// three;seven;eight;nine;two
//
// OUTPUT SAMPLE:
//
// Print numbers in the following way:
//
// 025784
// 37892
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

var words = map[string]string{"one": "1", "two": "2", "three": "3", "four": "4", "five": "5", "six": "6", "seven": "7", "eight": "8", "nine": "9", "zero": "0"}

func w2num(s string) string {
	digits := strings.Split(s, ";")
	for i, d := range digits {
		digits[i] = words[d]
	}
	return strings.Join(digits, "")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(w2num(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
