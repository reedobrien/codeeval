// MIXED CONTENT
// CHALLENGE DESCRIPTION:

// You have a string of words and digits divided by comma. Write a program which separates words with digits. You shouldn't change the order elements.

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Input example is the following

// 8,33,21,0,16,50,37,0,melon,7,apricot,peach,pineapple,17,21
// 24,13,14,43,41
// OUTPUT SAMPLE:

// melon,apricot,peach,pineapple|8,33,21,0,16,50,37,0,7,17,21
// 24,13,14,43,41
// As you cas see you need to output the same input string if it has words only or digits only.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"
)

func unmix(s string) string {
	var words, digits []string
	elems := strings.Split(s, ",")
	for _, e := range elems {
		r := []rune(e)[0]
		switch {
		case unicode.IsLetter(r):
			words = append(words, e)
		case unicode.IsDigit(r):
			digits = append(digits, e)
		}
	}
	return strings.Trim(fmt.Sprintf("%s|%s", strings.Join(words, ","), strings.Join(digits, ",")), "|")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(unmix(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
