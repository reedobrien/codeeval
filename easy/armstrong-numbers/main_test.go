package main

import "testing"

func TestIsArmstrong(t *testing.T) {
	values := []struct {
		in   string
		want string
	}{
		{"132", "False"},
		{"153", "True"},
		{"1620", "False"},
		{"1634", "True"},
		{"2030", "False"},
		{"370", "True"},
		{"371", "True"},
		{"407", "True"},
		{"4307", "False"},
		{"4415", "False"},
		{"5043", "False"},
		{"5115", "False"},
		{"5367", "False"},
		{"5550", "False"},
		{"6278", "False"},
		{"7089", "False"},
		{"7562", "False"},
		{"8208", "True"},
		{"8876", "False"},
		{"9474", "True"},
	}
	for _, tv := range values {
		got := isArmstrong(tv.in)
		if got != tv.want {
			t.Errorf("Got %s for %s but wanted %s", got, tv.in, tv.want)
		}
	}

}
