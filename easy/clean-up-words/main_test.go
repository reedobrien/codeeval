package main

import "testing"

func TestMersanne(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"(--9Hello----World...--)", "hello world"},
		{"Can 0$9 ---you~", "can you"},
		{"13What213are;11you-123+138doing7", "what are you doing"},
	}
	for _, test := range table {
		got := cleanUp(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
