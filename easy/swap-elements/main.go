// SWAP ELEMENTS
// CHALLENGE DESCRIPTION:

// You are given a list of numbers which is supplemented with positions that have to be swapped.

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Input example is the following

// 1 2 3 4 5 6 7 8 9 : 0-8
// 1 2 3 4 5 6 7 8 9 10 : 0-1, 1-3

// As you can see a colon separates numbers with positions.
// Positions start with 0.
// You have to process positions left to right. In the example above (2nd line) first you process 0-1, then 1-3.

// OUTPUT SAMPLE:

// Print the lists in the following way.

// 9 2 3 4 5 6 7 8 1
// 2 4 3 1 5 6 7 8 9 10
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func getInput(s string) ([]string, [][]int) {
	lr := strings.Split(s, ":")
	l, sstr := strings.TrimSpace(lr[0]), strings.TrimSpace(lr[1])
	list := strings.Split(l, " ")
	selems := strings.Split(sstr, ",")
	swaps := [][]int{}
	for _, e := range selems {
		pair := strings.Split(e, "-")
		i, err := strconv.Atoi(strings.TrimSpace(pair[0]))
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting string to int:", err)
			os.Exit(1)
		}
		j, err := strconv.Atoi(pair[1])
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting string to int:", err)
			os.Exit(1)
		}
		swaps = append(swaps, []int{i, j})
	}
	return list, swaps
}

func swap(s string) string {
	l, swaps := getInput(s)
	for _, swap := range swaps {
		l[swap[0]], l[swap[1]] = l[swap[1]], l[swap[0]]
	}
	return strings.Join(l, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(swap(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
