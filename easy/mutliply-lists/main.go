// MULTIPLY LISTS
// CHALLENGE DESCRIPTION:
//
// You have 2 lists of positive integers. Write a program which multiplies corresponding elements in these lists.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input example is the following
//
// 9 0 6 | 15 14 9
// 5 | 8
// 13 4 15 1 15 5 | 1 4 15 14 8 2
//
// The lists are separated with a pipe char, numbers are separated with a space char.
// The number of elements in lists are in range [1, 10].
// The number of elements is the same in both lists.
// Each element is a number in range [0, 99].
//
// OUTPUT SAMPLE:
//
// Print the result in the following way.
//
// 135 0 54
// 40
// 13 16 225 14 120 10
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting input to int:", err)
		os.Exit(1)
	}
	return i
}
func multiply(s string) string {
	var results []string
	split := strings.Split(s, "|")
	l := strings.Split(strings.TrimSpace(split[0]), " ")
	r := strings.Split(strings.TrimSpace(split[1]), " ")

	for i := range l {
		p := atoi(l[i]) * atoi(r[i])
		results = append(results, strconv.Itoa(p))
	}
	return strings.Join(results, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(multiply(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
