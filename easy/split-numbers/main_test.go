package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestSolution(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"3413289830 a-bcdefghij", "-413289827"},
		{"776 a+bc", "83"},
		{"12345 a+bcde", "2346"},
		{"1232 ab+cd", "44"},
		{"90602 a+bcde", "611"},
	}
	for _, tv := range values {
		equals(t, splitnum(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
