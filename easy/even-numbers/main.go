// Write a program which checks input numbers and determines whether a number is even or not.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input example is the following
//
// 701
// 4123
// 2936
//
// OUTPUT SAMPLE:
//
// Print 1 if the number is even or 0 if the number is odd.
//
// 0
// 0
// 1
// All numbers in input are integers > 0 and < 5000.
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func isEven(s string) string {
	n, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting string to number:", err)
		os.Exit(1)
	}
	if n%2 == 0 {
		return "1"
	}
	return "0"
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(isEven(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
