// NOT SO CLEVER
// CHALLENGE DESCRIPTION:
//
// Imagine that you have to arrange items in a certain order: pencils from
// black to white in a color palette, photographs by the date taken, banknotes
// from the highest to the lowest, etc. To do this, you definitely don’t need
// to use the Stupid sort algorithm.
//
// After each action, you need to come back to the beginning and start all over
// again. Not so clever, is it? But, you need to know about this algorithm,
// that’s why it is used in this challenge.
//
// INPUT SAMPLE:
// The first argument is a path to a file. Each line includes a test case which
// contains numbers that you need to sort using the Stupid sort algorithm.
// There is also a number of iterations for an algorithm to carry out. The
// numbers themselves and the number of iterations are separated by a pipeline
// '|'.
//
// 4 3 2 1 | 1
// 5 4 3 2 1 | 2
//
// OUTPUT SAMPLE:
// Print sorted numbers after they pass the required number of iterations. One
// iteration of this sort is a pass to the moment of making changes. Once
// changing the order of the digits, passing starts from the very beginning.
// Hence, this is another iteration.
//
// 3 4 2 1
// 4 3 5 2 1
//
// CONSTRAINTS:
// The number of iterations can be from 1 to 8.
// One iteration of this sort is a pass to the moment of making changes.
// The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func process(s string) string {
	data := strings.Split(s, "|")
	list := strings.Trim(data[0], " ")
	rotations := strings.Trim(data[1], " ")
	rot, err := strconv.Atoi(rotations)
	if err != nil {
		log.Fatalln("failed to parse rotations:", err)
	}
	var nums []int
	for _, n := range strings.Split(list, " ") {
		d, err := strconv.Atoi(n)
		if err != nil {
			log.Fatal(err)
		}
		nums = append(nums, d)
	}
	for i := 0; i < rot; i++ {
		nums = stupidSort(nums)
	}
	var res []string
	for _, d := range nums {
		res = append(res, strconv.Itoa(d))
	}
	return strings.Join(res, " ")
}

func stupidSort(i []int) []int {
	max := len(i)
	a, b := 0, 1
	for b < max {
		if i[b] < i[a] {
			i[a], i[b] = i[b], i[a]
			break
		}
		a, b = b, b+1
	}
	return i
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		log.Fatalln("error opening file:", err)
	}
	procLines(f)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(process(line))
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln("error opening file:", err)
	}
}
