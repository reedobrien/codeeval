// ROMAN NUMERALS
// CHALLENGE DESCRIPTION:

// Many persons are familiar with the Roman numerals for relatively small numbers. The symbols I (capital i), V, X, L, C, D, and M represent the decimal values 1, 5, 10, 50, 100, 500 and 1000 respectively. To represent other values, these symbols, and multiples where necessary, are concatenated, with the smaller-valued symbols written further to the right. For example, the number 3 is represented as III, and the value 73 is represented as LXXIII. The exceptions to this rule occur for numbers having units values of 4 or 9, and for tens values of 40 or 90. For these cases, the Roman numeral representations are IV (4), IX (9), XL (40), and XC (90). So the Roman numeral representations for 24, 39, 44, 49, and 94 are XXIV, XXXIX, XLIV, XLIX, and XCIV, respectively.

// Write a program to convert a cardinal number to a Roman numeral.

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Input example is the following

// 159
// 296
// 3992
// Input numbers are in range [1, 3999]

// OUTPUT SAMPLE:

// Print out Roman numerals.

// CLIX
// CCXCVI
// MMMCMXCII

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

var (
	magnitudes = []string{"I", "X", "C", "M"}
	halves     = []string{"V", "L", "D"}
)

func convert(s string) string {
	ln := len(s)
	roman := []string{}
	for i, v := range s {
		d, err := strconv.Atoi(string(v))
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting string to int:", err)
			os.Exit(1)
		}
		if d == 0 {
			continue
		}
		pos := ln - i
		switch d {
		case 9:
			roman = append(roman, magnitudes[pos-1]+magnitudes[pos])
		case 4:
			roman = append(roman, magnitudes[pos-1]+halves[pos-1])
		case 5:
			roman = append(roman, halves[pos-1])
		case 6, 7, 8:
			roman = append(roman, halves[pos-1]+strings.Repeat(magnitudes[pos-1], d-5))
		default:
			roman = append(roman, strings.Repeat(magnitudes[pos-1], d))
		}
	}
	return strings.Join(roman, "")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(convert(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
