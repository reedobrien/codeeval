package main

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestMove(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"XX.YY,XXX.Y,X..YY,XX..Y", "1"},
		{"XXX.YYYY,X...Y..Y,XX..YYYY,X.....YY,XX....YY", "1"},
		{"XX...YY,X....YY,XX..YYY,X..YYYY", "2"},
		{"XXYY,X..Y,XX.Y", "0"},
		{"XX...YY,X....YY,XX..YYY,XYYYY.Y", "0"},
		{"X...Y,X.YYY,XXYYY,X..YY,X..YY,XXYYY,XXYYY", "0"},
		{"XX.YYY,X.YYYY,X...YY,XXYYYY,XXXYYY,XXX.YY", "0"},
		{"XXXXXXX.YY,X......YYY,XXXXXXXYYY,XXXXXXXYYY,XXXXX.YYYY,XXXXXX...Y,X........Y,XXXXXXX..Y", "0"},
	}
	for _, tv := range values {
		equals(t, distance(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Fprintf(os.Stderr, "\033[31m%s:%d:\n\n\t got: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
