// RIGHTMOST CHAR
// CHALLENGE DESCRIPTION:
//
// You are given a string 'S' and a character 't'. Print out the position of
// the rightmost occurrence of 't' (case matters) in 'S' or -1 if there is
// none. The position to be printed out is zero based.
//
// INPUT SAMPLE:
//
// The first argument will ba a path to a filename, containing a string and a
// character, comma delimited, one per line. Ignore all empty lines in the
// input file. E.g.
//
// Hello World,r
// Hello CodeEval,E
//
// OUTPUT SAMPLE:
//
// Print out the zero based position of the character 't' in string 'S', one
// per line. Do NOT print out empty lines between your output.
// E.g.
//
// 8
// 10
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func getPosition(s, t string) int {
	return strings.LastIndex(s, t)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) > 0 {
			elems := strings.Split(line, ",")
			s, t := elems[0], elems[1]
			pos := getPosition(s, t)
			fmt.Fprintln(os.Stdout, pos)
		}
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
