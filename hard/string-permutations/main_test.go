package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestPermutations(t *testing.T) {
	values := []struct {
		in   string
		want string
	}{
		{"hat", "aht,ath,hat,hta,tah,tha"},
		{"abc", "abc,acb,bac,bca,cab,cba"},
		{"Zu6", "6Zu,6uZ,Z6u,Zu6,u6Z,uZ6"},
	}
	for _, tv := range values {
		var permutations []string
		equals(t, permutation(tv.in, 0, &permutations), tv.want)
	}
}

// equals fails the test if got is not equal to want.
func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot:  %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
