// Write a program which capitalizes the first letter of each word in a sentence.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input example is the following
//
// Hello world
// javaScript language
// a letter
// 1st thing
//
// OUTPUT SAMPLE:
//
// Print capitalized words in the following way.
//
// Hello World
// JavaScript Language
// A Letter
// 1st Thing
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(strings.Title(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
