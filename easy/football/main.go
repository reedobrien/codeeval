// FOOTBALL
// CHALLENGE DESCRIPTION:
//
// People around the world watch football matches and root for different
// football teams. Some people are fans of Real Madrid, some like Barcelona,
// some pull for Atletico Madrid, while others do not miss a single match with
// FC Bayern Munich.
// The teams would like to know people in which countries cheer for them. So,
// let’s help them!
//
// INPUT SAMPLE:
// The first argument is a path to a file. Each row includes a test case with
// lists of countries. Lists are separated by pipelines '|'. Each list includes
// football teams that people in these countries root for.
//
// For example:
//
// 1 2 3 4 | 3 1 | 4 1
// 19 11 | 19 21 23 | 31 39 29
//
// OUTPUT SAMPLE:
// For each football team, print a list of countries where people root for
// them. Separate each team by a semicolon ';' and a space. All output should
// be sorted.
//
// For example:
//
// 1:1,2,3; 2:1; 3:1,2; 4:1,3;
// 11:1; 19:1,2; 21:2; 23:2; 29:3; 31:3; 39:3;
//
// CONSTRAINTS:
// The number of countries lists can be from 3 to 20.
// Each list contains a different number of football teams: from 1 to 7.
// The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func process(s string) string {
	teamCountries := make(map[int][]int)

	countries := strings.Split(s, "|")
	for i, country := range countries {
		cid := i + 1
		clubs := strings.Split(strings.Trim(country, " "), " ")
		for _, club := range clubs {
			team, err := strconv.Atoi(club)
			if err != nil {
				log.Fatalf("error converting team %s for country %d", club, cid)
			}
			teamCountries[team] = append(teamCountries[team], cid)
		}
	}
	results := []string{}
	keys := []int{}
	for k, _ := range teamCountries {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for _, k := range keys {
		t := strconv.Itoa(k)
		countries := teamCountries[k]
		sort.Ints(countries)
		strCountries := []string{}
		for _, t := range countries {
			strCountries = append(strCountries, strconv.Itoa(t))
		}
		results = append(results, fmt.Sprintf("%s:%s;", t, strings.Join(strCountries, ",")))
	}
	return strings.Join(results, " ")
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		log.Fatalln("error opening file:", err)
	}
	procLines(f)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(process(line))
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln("error opening file:", err)
	}
}
