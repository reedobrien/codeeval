package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestDelta(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"14:01:57 12:47:11", "01:14:46"},
		{"13:09:42 22:16:15", "09:06:33"},
		{"08:08:06 08:38:28", "00:30:22"},
		{"23:35:07 02:49:59", "20:45:08"},
		{"14:31:45 14:46:56", "00:15:11"},
	}
	for _, tv := range values {
		equals(t, delta(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
