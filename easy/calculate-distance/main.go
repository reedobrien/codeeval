// You have coordinates of 2 points and need to find the distance between them.
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Input example is the following
//
// (25, 4) (1, -6)
// (47, 43) (-25, -11)
//
// All numbers in input are integers between -100 and 100.
//
// OUTPUT SAMPLE:
//
// Print results in the following way.
//
// 26
// 90
// You don't need to round the results you receive. They must be integer numbers.
package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting string to number:", err)
		os.Exit(1)
	}
	return i
}

type point struct {
	X, Y int
}

func newPoint(s string) point {
	s = strings.Replace(s, " ", "", -1)
	xy := strings.Split(s, ",")
	return point{X: atoi(xy[0]), Y: atoi(xy[1])}
}

func distance(s string) int {
	s = strings.Trim(s, "()")
	points := strings.Split(s, ") (")
	p1 := newPoint(points[0])
	p2 := newPoint(points[1])
	d := math.Sqrt(math.Pow(float64(p2.X-p1.X), 2) + math.Pow(float64(p2.Y-p1.Y), 2))
	return int(d)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(distance(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
