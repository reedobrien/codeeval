// Print out the sum of integers read from a file.
//
// INPUT SAMPLE:
//
// The first argument to the program will be a path to a filename containing a
// positive integer, one per line. E.g.
//
// 5
// 12
//
// OUTPUT SAMPLE:
//
// Print out the sum of all the integers read from the file. E.g.
//
// 17
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func procLines(f io.Reader) {
	var Σ int
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		digit := scanner.Text()
		i, err := strconv.Atoi(digit)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting characters to int:", err)
		}

		Σ += i
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	fmt.Fprintln(os.Stdout, Σ)

}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
