// Credits: This problem appeared in the Facebook Hacker Cup 2013 Hackathon.
//
// When John was a little kid he didn't have much to do. There was no internet,
// no Facebook, and no programs to hack on. So he did the only thing he
// could... he evaluated the beauty of strings in a quest to discover the most
// beautiful string in the world.
//
// Given a string s, little Johnny defined the beauty of the string as the sum
// of the beauty of the letters in it. The beauty of each letter is an integer
// between 1 and 26, inclusive, and no two letters have the same beauty. Johnny
// doesn't care about whether letters are uppercase or lowercase, so that
// doesn't affect the beauty of a letter. (Uppercase 'F' is exactly as
// beautiful as lowercase 'f', for example.)
//
// You're a student writing a report on the youth of this famous hacker. You
// found the string that Johnny considered most beautiful. What is the maximum
// possible beauty of this string?
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Each
// line in this file has a sentence. E.g.
//
// ABbCcc
// Good luck in the Facebook Hacker Cup this year!
// Ignore punctuation, please :)
// Sometimes test cases are hard to make up.
// So I just go consult Professor Dalves
//
// OUTPUT SAMPLE:
//
// Print out the maximum beauty for the string. E.g.
// 152
// 754
// 491
// 729
// 646
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
	"unicode"
)

type letter struct {
	L     string
	Count int
}

type letters []letter

func (l letters) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l letters) Less(i, j int) bool { return l[i].Count < l[j].Count }
func (l letters) Len() int           { return len(l) }

func (l letters) Score() int {
	var Σ int
	for i, letter := range l {
		Σ += (26 - i) * letter.Count
	}
	return Σ
}

func beauty(s string) int {
	var (
		i int
	)
	s = strings.ToLower(s)
	counter := make(map[string]int)
	chars := strings.Split(s, "")
	for _, c := range chars {
		if unicode.IsLetter([]rune(c)[0]) {
			counter[string(c)]++
		}
	}
	ll := make(letters, len(counter))
	for k, v := range counter {
		ll[i] = letter{L: k, Count: v}
		i++
	}
	sort.Sort(sort.Reverse(ll))
	return ll.Score()
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(beauty(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
