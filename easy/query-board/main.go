// There is a board (matrix). Every cell of the board contains one integer,
// which is 0 initially.
//
// The next operations can be applied to the Query Board:
// SetRow i x: it means that all values in the cells on row "i" have been
// changed to value "x" after this operation.
// SetCol j x: it means that all values in the cells on column "j" have been
// changed to value "x" after this operation.
// QueryRow i: it means that you should output the sum of values on row "i".
// QueryCol j: it means that you should output the sum of values on column "j".
//
// The board's dimensions are 256x256
// "i" and "j" are integers from 0 to 255
// "x" is an integer from 0 to 31
//
// INPUT SAMPLE:
//
// Your program should accept as its first argument a path to a filename. Each
// line in this file contains an operation of a query. E.g.
//
// SetCol 32 20
// SetRow 15 7
// SetRow 16 31
// QueryCol 32
// SetCol 2 14
// QueryRow 10
// SetCol 14 0
// QueryRow 15
// SetRow 10 1
// QueryCol 2
//
// OUTPUT SAMPLE:
//
// For each query, output the answer of the query. E.g.
//
// 5118
// 34
// 1792
// 3571
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type board [256][256]int

func newBoard() board {
	b := board{}
	return b
}

func (b *board) SetRow(i, x int) {
	for c := range b[i] {
		b[i][c] = x
	}
}

func (b *board) SetCol(j, x int) {
	for r := range b {
		b[r][j] = x
	}
}

func (b *board) QueryRow(i int) int {
	var Σ int
	for c := range b[i] {
		Σ += b[i][c]
	}
	return Σ
}

func (b *board) QueryCol(j int) int {
	var Σ int
	for r := range b {
		Σ += b[r][j]
	}
	return Σ
}

func atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting input to int:", err)
	}
	return i
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	b := newBoard()
	for scanner.Scan() {
		line := scanner.Text()
		elems := strings.Split(line, " ")
		cmd, vals := elems[0], elems[1:]
		switch {
		case cmd == "SetRow":
			b.SetRow(atoi(vals[0]), atoi(vals[1]))
		case cmd == "SetCol":
			b.SetCol(atoi(vals[0]), atoi(vals[1]))
		case cmd == "QueryRow":
			fmt.Println(b.QueryRow(atoi(vals[0])))
		case cmd == "QueryCol":
			fmt.Println(b.QueryCol(atoi(vals[0])))
		}
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
