package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"Vampires: 1, Zombies: 1, Witches: 1, Houses: 1", "4"},
		{"Vampires: 3, Zombies: 2, Witches: 1, Houses: 10", "36"},
	}
	for i, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Errorf("test %d: %s got: %s wanted: %s", i+1, test.in, got, test.want)
		}
	}
}
