// Players generally sit in a circle. The first player says the number “1”, and
// each player says next number in turn. However, any number divisible by X
// (for example, three) is replaced by the word fizz, and any divisible by Y
// (for example, five) by the word buzz. Numbers divisible by both become fizz
// buzz. A player who hesitates, or makes a mistake is eliminated from the
// game.
//
// Write a program that prints out the final series of numbers where those
// divisible by X, Y and both are replaced by “F” for fizz, “B” for buzz and
// “FB” for fizz buzz.
//
// INPUT SAMPLE:
//
// Your program should accept a file as its first argument. The file contains
// multiple separated lines; each line contains 3 numbers that are space
// delimited. The first number is the first divider (X), the second number is
// the second divider (Y), and the third number is how far you should count
// (N). You may assume that the input file is formatted correctly and the
// numbers are valid positive integers.
//
// For example:
//
//
// 3 5 10
// 2 7 15
//
//OUTPUT SAMPLE:
//
// Print out the series 1 through N replacing numbers divisible by X with “F”,
// numbers divisible by Y with “B” and numbers divisible by both with “FB”.
// Since the input file contains multiple sets of values, your output should
// print out one line per set. Ensure that there are no trailing empty spaces
// in each line you print.
//
//
// 1 2 F 4 B F 7 8 F B
// 1 F 3 F 5 F B F 9 F 11 F 13 FB 15
//
// CONSTRAINTS:
//
// The number of test cases ≤ 20 "X" is in range [1, 20] "Y" is in range [1,
// 20] "N" is in range [21, 100]
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type game struct {
	X, Y, N int
}

func (g game) Play() string {
	var results []string
	for i := 1; i <= g.N; i++ {
		switch {
		case i%g.X == 0 && i%g.Y == 0:
			results = append(results, "FB")
			break
		case i%g.X == 0:
			results = append(results, "F")
			break
		case i%g.Y == 0:
			results = append(results, "B")
			break
		default:
			results = append(results, strconv.Itoa(i))
		}
	}
	return strings.Join(results, " ")
}

func newGame(in string) game {
	input := strings.Split(in, " ")
	x, err := strconv.Atoi(input[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting string to int for X: %s\n", err)
		os.Exit(1)
	}
	y, err := strconv.Atoi(input[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting string to int for Y: %s\n", err)
		os.Exit(1)
	}
	max, err := strconv.Atoi(input[2])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting string to int for N: %s\n", err)
		os.Exit(1)
	}

	return game{X: x, Y: y, N: max}
}

func playgames(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		g := newGame(scanner.Text())
		outcome := g.Play()
		fmt.Fprintln(os.Stdout, outcome)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	playgames(f)
}
