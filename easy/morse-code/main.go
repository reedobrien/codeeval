// MORSE CODE
// CHALLENGE DESCRIPTION:

// You have received a text encoded with Morse code and want to decode it.

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Input example is the following:

// .- ...- ..--- .-- .... .. . -.-. -..-  ....- .....
// -... .... ...--
// Each letter is separated by space char, each word is separated by 2 space chars.

// OUTPUT SAMPLE:

// Print out decoded words. E.g.

// AV2WHIECX 45
// BH3

// You program has to support letters and digits only.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

var code = make(map[string]string)

func init() {
	for k, v := range revcode {
		code[v] = k
	}
}

func decode(s string) string {
	decoded := []string{}
	words := strings.Split(s, "  ")
	for _, candidate := range words {
		word := []string{}
		letters := strings.Split(candidate, " ")
		for _, l := range letters {
			word = append(word, code[l])
		}
		decoded = append(decoded, strings.Join(word, ""))
	}
	return strings.Join(decoded, " ")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(decode(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}

var revcode = map[string]string{
	"A": ".-",
	"B": "-...",
	"C": "-.-.",
	"D": "-..",
	"E": ".",
	"F": "..-.",
	"G": "--.",
	"H": "....",
	"I": "..",
	"J": ".---",
	"K": "-.-",
	"L": ".-..",
	"M": "--",
	"N": "-.",
	"O": "---",
	"P": ".--.",
	"Q": "--.-",
	"R": ".-.",
	"S": "...",
	"T": "-",
	"U": "..-",
	"V": "...-",
	"W": ".--",
	"X": "-..-",
	"Y": "-.--",
	"Z": "--..",
	"1": ".----",
	"2": "..---",
	"3": "...--",
	"4": "....-",
	"5": ".....",
	"6": "-....",
	"7": "--...",
	"8": "---..",
	"9": "----.",
	"0": "-----",
}
