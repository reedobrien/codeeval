package main

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestMove(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"4 3 3 5 7", "6"},
		{"3 20 30 40", "20"},
	}
	for _, tv := range values {
		equals(t, distance(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Fprintf(os.Stderr, "\033[31m%s:%d:\n\n\t got: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
