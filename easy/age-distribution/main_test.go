package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestDist(t *testing.T) {
	values := []struct {
		in, want string
	}{
		// 0
		// 19

		// OUTPUT SAMPLE:

		// For each line of input print out where the person is:

		// Still in Mama's arms
		// College

		{"0", "Still in mama's arms"},
		{"19", "College"},
	}
	for _, tv := range values {
		equals(t, dist(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
