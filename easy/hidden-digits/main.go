// HIDDEN DIGITS
// CHALLENGE DESCRIPTION:

// In this challenge you're given a random string containing hidden and visible
// digits. The digits are hidden behind lower case latin letters as follows: 0
// is behind 'a', 1 is behind ' b ' etc., 9 is behind 'j'. Any other symbol in
// the string means nothing and has to be ignored. So the challenge is to find
// all visible and hidden digits in the string and print them out in order of
// their appearance.

// INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Each
// line in this file contains a string. You may assume that there will be no
// white spaces inside the string. E.g.

// abcdefghik
// Xa,}A#5N}{xOBwYBHIlH,#W
// (ABW>'yy^'M{X-K}q,
// 6240488

// OUTPUT SAMPLE:

// For each test case print out all visible and hidden digits in order of their
// appearance. Print out NONE in case there is no digits in the string. E.g.

// 012345678
// 05
// NONE
// 6240488
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"
)

var hidden = map[string]string{
	"a": "0",
	"b": "1",
	"c": "2",
	"d": "3",
	"e": "4",
	"f": "5",
	"g": "6",
	"h": "7",
	"i": "8",
	"j": "9",
}

func numbers(s string) string {
	out := []string{}
	for _, r := range s {
		switch {
		case unicode.IsDigit(r):
			out = append(out, string(r))
		case unicode.IsLower(r):
			v, ok := hidden[string(r)]
			if ok {
				out = append(out, v)
			}
		}
	}
	if len(out) == 0 {
		return "NONE"
	}
	return strings.Join(out, "")
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(numbers(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
