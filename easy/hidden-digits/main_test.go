package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestNumbers(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"abcdefghik", "012345678"},
		{"Xa,}A#5N}{xOBwYBHIlH,#W", "05"},
		{"(ABW>'yy^'M{X-K}q,", "NONE"},
		{"6240488", "6240488"},
	}
	for _, tv := range values {
		equals(t, numbers(tv.in), tv.want)
	}
}

// equals fails the test if got is not equal to want.
func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot:  %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
