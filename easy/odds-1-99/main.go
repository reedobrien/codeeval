// Print the odd numbers from 1 to 99.

// INPUT SAMPLE:

// There is no input for this program.

// OUTPUT SAMPLE:

// Print the odd numbers from 1 to 99, one number per line.
package main

import "fmt"

func main() {
	for i := 1; i < 100; i += 2 {
		fmt.Println(i)
	}
}
