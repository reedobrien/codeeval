package main

import "testing"

func TestCountArrows(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"<--<<--<<", "2"},
		{"<<>>--><--<<--<<>>>--><", "4"},
		{"<-->>", "0"},
	}
	for _, test := range table {
		got := countArrows(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
