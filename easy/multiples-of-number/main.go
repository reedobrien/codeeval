// Given numbers x and n, where n is a power of 2, print out the smallest
// multiple of n which is greater than or equal to x. Do not use division or
// modulo operator.
//
// INPUT SAMPLE:
//
// The first argument will be a path to a filename containing a comma separated
// list of two integers, one list per line. E.g.
//
// 13,8
// 17,16
//
// OUTPUT SAMPLE:
//
// Print to stdout, the smallest multiple of n which is greater than or equal to x, one per line. E.g.
//
// 16
// 32
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type entry struct {
	X, N int
}

func (e entry) SmallestMultiple() int {
	if e.N > e.X {
		return e.N
	}
	var p int
	for i := 2; ; i++ {
		p = e.N * i
		if p > e.X {
			return p
		}
	}
}

func newEntry(l string) entry {
	eArr := strings.Split(l, ",")
	x, err := strconv.Atoi(eArr[0])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting x to int:", err)
		os.Exit(1)
	}
	n, err := strconv.Atoi(eArr[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting n to int:", err)
		os.Exit(1)
	}
	return entry{X: x, N: n}
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		entry := scanner.Text()
		e := newEntry(entry)
		fmt.Fprintln(os.Stdout, e.SmallestMultiple())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
