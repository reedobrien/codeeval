// Compare Points

// Challenge Description:

// Bob's hiking club is lost in the mountains on the way to a scenic overlook.
// Fortunately, Bob has a GPS device, so that he can see the coordinates where
// the group is currently at. The GPS gives the current X/Y coordinates as O,
// P, and the scenic overlook is located at Q, R. Bob now just needs to tell
// the group which way to go so they can get to the overlook in time for
// s'mores.
// Input sample:

// 0 0 1 5
// 12 13 12 13
// 0 1 0 5

// The input is a file with each line representing a test case. Each test case
// consists of four integers O, P, Q, R on a line, separated by spaces.
// Output sample:

// NE
// here
// N

// For each test case print a line containing one of the following: N, NE, E,
// SE, S, SW, W, NW, here if the coordinates Q, R are (respectively) north,
// northeast, east, southeast, south, southwest, west, northwest, or already at
// ("here") the coordinates O, P. Note that N, S, E and W mean directly North,
// South, East or West respectively, i.e. X or Y coordinates of two points are
// exactly the same. In all other cases your output should be one of the NW,
// NE, SW, SE or here.
//Constraints:

//     All coordinates -10000 < |O,P,Q,R| < 10000
//     Number of test cases is 40
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	o int = iota
	p
	q
	r
)

func countArrows(s string) string {
	var (
		coords [4]int
	)
	coordStr := strings.Split(s, " ")
	for i := 0; i < 4; i++ {
		v, err := strconv.Atoi(coordStr[i])
		if err != nil {
			log.Fatalf("Failed to convert %s to int\n", coordStr[i])
		}
		coords[i] = v
	}
	if coords[o] == coords[q] && coords[p] == coords[r] {
		return "here"
	}
	var lat, lon string
	if coords[o] < coords[q] {
		lon = "E"
	}
	if coords[o] > coords[q] {
		lon = "W"
	}
	if coords[p] < coords[r] {
		lat = "N"
	}
	if coords[p] > coords[r] {
		lat = "S"
	}

	return lat + lon
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(countArrows(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
