package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestDist(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"330.39991833", "330.23'59\""},
		{"0.001", "0.00'03\""},
		{"14.64530319", "14.38'43\""},
		{"0.25", "0.15'00\""},
		{"254.16991217", "254.10'11\""},
	}
	for _, tv := range values {
		equals(t, dangle(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
