// Strings and arrows

// Challenge Description:

// You have a string composed of the following symbols: '>', '<', and '-'. Your
// task is to find, count, and print to the output a number of arrows in the
// string. An arrow is a set of the following symbols: '>>-->' or '<--<<'.
// Note that one character may belong to two arrows at the same time. Such
// example is shown in the line #1.
// Input sample:

// <--<<--<<
// <<>>--><--<<--<<>>>--><
// <-->>

// The first argument is a path to a file. Each line includes a test case with
// a string of different length from 10 to 250 characters. The string consists
// of '>', '<', and '-' symbols.

// For example:
// Output sample:

// 2
// 4
// 0

// Print the total number of found arrows for each test case.

// For example:
// Constraints:

//     An arrow is a set of the following symbols: '>>-->' or '<--<<'.
//     One symbol may belong to two arrows at the same time.
//     The number of test cases is 40.

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

const (
	arrow  = ">>-->"
	larrow = "<--<<"
)

func countArrows(s string) string {
	var count int
	for i := 0; i < len(s)-4; i++ {
		switch s[i] {
		case '>':
			if s[i:i+5] == arrow {
				count++
			}
		case '<':
			if s[i:i+5] == larrow {
				count++
			}
		}
	}
	return strconv.Itoa(count)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(countArrows(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
