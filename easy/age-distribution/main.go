// CHALLENGE DESCRIPTION:

// You're responsible for providing a demographic report for your local school
// district based on age. To do this, you're going determine which 'category'
// each person fits into based on their age.
// The person's age will determine which category they should be in:

// If they're from 0 to 2 the child should be with parents print : 'Still in Mama's arms'
// If they're 3 or 4 and should be in preschool print : 'Preschool Maniac'
// If they're from 5 to 11 and should be in elementary school print : 'Elementary school'

// From 12 to 14: 'Middle school'
// From 15 to 18: 'High school'
// From 19 to 22: 'College'
// From 23 to 65: 'Working for the man'
// From 66 to 100: 'The Golden Years'

// If the age of the person less than 0 or more than 100 - it might be an alien
// - print: "This program is for humans" INPUT SAMPLE:

// Your program should accept as its first argument a path to a filename. Each
// line of input contains one integer - age of the person:

// 0
// 19

// OUTPUT SAMPLE:

// For each line of input print out where the person is:

// Still in Mama's arms
// College
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
)

func dist(l string) string {
	age, err := strconv.Atoi(l)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error converting string to int:", err.Error())
		os.Exit(1)
	}
	var resp string
	switch {
	case age < 0 || age > 100:
		resp = "This program is for humans"
	case age < 3:
		resp = "Still in mama's arms"
	case age < 5:
		resp = "Preschool Maniac"
	case age < 12:
		resp = "Elementary school"
	case age < 15:
		resp = "Middle school"
	case age < 19:
		resp = "High school"
	case age < 23:
		resp = "College"
	case age < 66:
		resp = "Working for the man"
	case age < 101:
		resp = "The Golden Years"
	}
	return resp
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(dist(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
