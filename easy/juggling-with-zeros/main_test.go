package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestRecovery(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"00 0 0 00 00 0", "9"},
		{"00 0", "1"},
		{"00 0 0 000 00 0000000 0 000", "9208"},
		{"0 000000000 00 00", "3"},
	}
	for _, tv := range values {
		equals(t, juggle(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
