package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func fibonacci(n int) int {
	if n == 0 || n == 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		numStr := scanner.Text()
		num, err := strconv.Atoi(numStr)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error converting input line to int:", err)
		}
		fmt.Fprintln(os.Stdout, fibonacci(num))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
