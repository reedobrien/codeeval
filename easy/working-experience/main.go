// Working experience

// Challenge Description:

// You are building a new social platform and want to store user's work
// experience. You have decided to calculate the total experience of each user
// in years based on the time periods that they provided. Using this approach,
// you need to be sure that you are taking into account the overlapping time
// periods in order to retrieve the actual work experience in years.

// For example:

// Jan 2010-Dec 2010
// Jan 2010-Dec 2010

// Two jobs with 12 months of experience each, but actual work experience is 1
// year because of the overlapping time periods. The task is to calculate the
// actual work experience based on the list of time intervals.

// Input sample:
// Your program should accept a path to a filename as its first argument. Each
// line of the file contains a list of time periods separated by a semicolon
// and a single space. Each time period is represented as the begin date and
// the end date. Each date consists of a month as an abbreviated name and a
// year with century as a decimal number separated by a single space. The begin
// date and the end date are separated by a hyphen.

// For example:
// Feb 2004-Dec 2009; Sep 2004-Jul 2008
// Aug 2013-Mar 2014; Apr 2013-Aug 2013; Jun 2014-Aug 2015; Apr 2003-Nov 2004; Apr 2014-Jan 2015
// Mar 2003-Jul 2003; Nov 2003-Jan 2004; Apr 1999-Nov 1999
// Apr 1992-Dec 1993; Feb 1996-Sep 1997; Jan 2002-Jun 2002; Sep 2003-Apr 2004; Feb 2010-Nov 2011
// Feb 2004-May 2004; Jun 2004-Jul 2004

// Output sample:
// 5
// 4
// 1
// 6
// 0

// Print out the actual work experience in years for each test case.

// For example:
// Constraints:

//     The number of lines in a file is in a range from 20 to 40.
//     The dates are in a range from Jan 1990 to Dec 2020.
//     The end date is greater than the begin date.
//     The begin date is the first day of a given month, and the end date is the last day of a given month.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strings"
	"time"
)

var format = "Jan 2006"

type transition struct {
	Time  time.Time
	Start bool
}

func newTransition(s string, start bool) transition {
	t, err := time.Parse(format, s)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing date from string:", err.Error())
		os.Exit(1)
	}
	return transition{t, start}
}

type transitions []transition

func (t transitions) Less(i, j int) bool { return t[i].Time.Before(t[j].Time) }
func (t transitions) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }
func (t transitions) Len() int           { return len(t) }

type period struct {
	Start, End time.Time
}

func (p period) Duration() time.Duration {
	return p.End.Sub(p.Start)
}

func newPeriod(start, end string) period {
	s, err := time.Parse(format, start)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing date from string:", err.Error())
		os.Exit(1)
	}
	e, err := time.Parse(format, end)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing date from string:", err.Error())
		os.Exit(1)
	}
	return period{s, e}
}

func calc(l string) string {
	changes := []transition{}
	entries := strings.Split(l, "; ")
	// create slice of transitions (epochs)
	for _, e := range entries {
		entry := strings.Split(e, "-")
		changes = append(changes, newTransition(entry[0], true))
		changes = append(changes, newTransition(entry[1], false))
	}
	// sort them
	sort.Sort(transitions(changes))

	periods := &[]period{}
	// bracket counting algorithm
	count(changes, periods)

	var m float64
	for _, p := range *periods {
		// take seconds and calculate months in duration
		// sadly I don't think I know why this works...
		m += round((p.Duration().Seconds() / 3.15569e7 * 12) + 1)
	}
	return fmt.Sprintf("%d", int(m)/12)
}

func round(input float64) float64 {
	if input < 0 {
		return math.Ceil(input - 0.5)
	}
	return math.Floor(input + 0.5)
}

func count(changes []transition, periods *[]period) {
	var counter int
	var start, end time.Time
	if changes[0].Start {
		start = changes[0].Time
	} else {
		fmt.Fprintln(os.Stderr, "The 0 element should be a starting time but wasn't: ", changes[0], counter)
		os.Exit(1)
	}

	for i, t := range changes {
		if t.Start {
			counter++
		} else {
			counter--
		}
		if counter == 0 {
			end = t.Time
			*periods = append(*periods, period{start, end})
			if i+1 < len(changes) {
				count(changes[i+1:], periods)
				return
			}
		}

	}

}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(calc(line))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input: ", err)
	}
}

func main() {
	flag.Parse()
	fpath := flag.Arg(0)
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
