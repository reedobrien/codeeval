package main

import "testing"

func TestCountArrows(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"hello 11001", "HEllO"},
		{"world 10000", "World"},
		{"cba 111", "CBA"},
	}
	for _, test := range table {
		got := unmask(test.in)
		if got != test.want {
			t.Fatalf(
				"testing %s failed, got %q, want %q\n", test.in, got, test.want)
		}
	}
}
