package main

import "testing"

func TestProcess(t *testing.T) {
	table := []struct {
		in, want string
	}{
		{"9999 9999 9999 9999", "Fake"},
		{"9999 9999 9999 9993", "Real"},
	}
	for i, test := range table {
		got := process(test.in)
		if got != test.want {
			t.Errorf("test %d for value %s, got: %s, want: %s", i, test.in, got, test.want)
		}
	}
}
