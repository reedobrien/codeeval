// You are given a sorted list of numbers with duplicates. Print out the sorted
// list with duplicates removed.
//
// INPUT SAMPLE:
//
// File containing a list of sorted integers, comma delimited, one per line.
// E.g.
//
// 1,1,1,2,2,3,3,4,4
// 2,3,4,5,5
//
// OUTPUT SAMPLE:
//
// Print out the sorted list with duplicates removed, one per line.
// E.g.
//
// 1,2,3,4
// 2,3,4,5
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		var (
			dMap   = make(map[string]bool)
			unique []string
		)
		digits := strings.Split(scanner.Text(), ",")

		for _, d := range digits {
			if !dMap[d] {
				dMap[d] = true
				unique = append(unique, d)
			}
		}
		fmt.Println(strings.Join(unique, ","))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file: ", err.Error())
		os.Exit(1)
	}
	procLines(f)
}
