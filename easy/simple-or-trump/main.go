// SIMPLE OR TRUMP
// CHALLENGE DESCRIPTION:
//
// First playing cards were invented in Eastern Asia and then spread all over
// the world taking different forms and appearance. In India, playing cards
// were round, and they were called Ganjifa. In medieval Japan, there was a
// popular Uta-garuta game, in which shell mussels were used instead of playing
// cards.
// In our game, we use playing cards that are more familiar nowadays. The rules
// are also simple: an ace beats a deuce (2) unless it is a trump deuce.
//
// INPUT SAMPLE:
// The first argument is a path to a file. Each line includes a test case which
// contains two playing cards and a trump suit. Cards and a trump suite are
// separated by a pipeline (|). The card deck includes thirteen ranks (from a
// deuce to an ace) of each of the four suits: clubs (♣), diamonds (♦), hearts
// (♥), and spades (♠). There are no Jokers.
//
// AD 2H | H
// KD KH | C
// JH 10S | C
//
// OUTPUT SAMPLE:
// Your task is to print a card that wins. If there is no trump card, then the
// higher card wins. If the cards are equal, then print both cards.
//
// 2H
// KD KH
// JH
// CONSTRAINTS:
//
// The card deck includes ranks from a deuce (2) to an ace, no Jokers.
// If the cards are equal, then print both cards.
// The number of test cases is 40.
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	TWO = iota + 2
	THREE
	FOUR
	FIVE
	SIX
	SEVEN
	EIGHT
	NINE
	TEN
	JACK
	QUEEN
	KING
	ACE
)

func newCard(s, t string) card {
	return card{rank: string(s[:len(s)-1]), suit: string(s[len(s)-1]), trump: t}
}

type card struct {
	rank, suit, trump string
}

func (c card) IsTrump() bool {
	return c.suit == c.trump
}

func (c card) Rank() int {
	switch c.rank {
	case "J":
		return JACK
	case "Q":
		return QUEEN
	case "K":
		return KING
	case "A":
		return ACE
	default:
		i, err := strconv.Atoi(c.rank)
		if err != nil {
			log.Fatalln(err)
		}
		return i
	}
}

func (c card) String() string {
	return fmt.Sprintf("%s%s", c.rank, c.suit)
}

func process(s string) string {
	data := strings.Split(s, "|")
	cards := strings.Split(strings.Trim(data[0], " "), " ")
	trump := strings.Trim(data[1], " ")
	a := newCard(strings.Trim(cards[0], " "), trump)
	b := newCard(strings.Trim(cards[1], " "), trump)

	if a.IsTrump() {
		if b.IsTrump() {
			if a.Rank() < b.Rank() {
				return b.String()
			}
		}
		return a.String()
	}
	if b.IsTrump() {
		return b.String()
	}
	if a.Rank() > b.Rank() {
		return a.String()
	}
	if a.Rank() < b.Rank() {
		return b.String()
	}
	return fmt.Sprintf("%s %s", a, b)
}

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	defer f.Close()
	if err != nil {
		log.Fatalln("error opening file:", err)
	}
	procLines(f)
}

func procLines(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(process(line))
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln("error opening file:", err)
	}
}
