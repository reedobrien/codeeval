package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestShortestRep(t *testing.T) {
	testValues := []struct {
		in   string
		want int
	}{

		{"abcabcabcabc", 3},
		{"bcbcbcbcbcbcbcbcbcbcbcbcbcbc", 2},
		{"dddddddddddddddddddd", 1},
		{"adcdefg", 7},
	}
	for _, tv := range testValues {
		got := shortestRep(tv.in)
		equals(t, got, tv.want)
	}
}

// equals fails the test if got is not equal to want.
func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot:  %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
