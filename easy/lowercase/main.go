// Given a string write a program to convert it into lowercase.
//
// INPUT SAMPLE:
//
// The first argument will be a path to a filename containing sentences, one
// per line. You can assume all characters are from the english language. E.g.
//
// HELLO CODEEVAL
// This is some text
//
//OUTPUT SAMPLE:
//
// Print to stdout, the lowercase version of the sentence, each on a new line.
// E.g.
//
// hello codeeval
//this is some text
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fpath := os.Args[1]
	f, err := os.Open(fpath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open file: %s", err)
	}
	lowerit(f)
}

func lowerit(f io.Reader) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		fmt.Println(strings.ToLower(scanner.Text()))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading input:", err)
	}
}
