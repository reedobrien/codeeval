package main

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestRecovery(t *testing.T) {
	values := []struct {
		in, want string
	}{
		{"thisTHIS", "lowercase: 50.00 uppercase: 50.00"},
		{"AAbbCCDDEE", "lowercase: 20.00 uppercase: 80.00"},
		{"N", "lowercase: 0.00 uppercase: 100.00"},
		{"UkJ", "lowercase: 33.33 uppercase: 66.67"},
	}
	for _, tv := range values {
		equals(t, calc(tv.in), tv.want)
	}
}

func equals(tb testing.TB, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}
